<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ThongBao */
?>
<div class="thong-bao-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'noi_dung:ntext',
            'created',
            'time_seen',
            'is_seen:boolean',
            'user_id',
            'user_created_id',
        ],
    ]) ?>

</div>
