<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\BinhLuanPhanAnh */
?>
<div class="binh-luan-phan-anh-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'noi_dung:ntext',
            'created',
            'hinh_anh',
            'active:boolean',
            'phan_anh_hien_truong_id',
        ],
    ]) ?>

</div>
