<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "vq_lop".
 *
 * @property int $id
 * @property string|null $name
 * @property int $si_so
 * @property int|null $lop_truong_id
 * @property int|null $khoa_id
 *
 * @property Khoa $khoa
 * @property User $lopTruong
 */
class Lop extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vq_lop';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['si_so'], 'required'],
            [['si_so', 'lop_truong_id', 'khoa_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['khoa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Khoa::className(), 'targetAttribute' => ['khoa_id' => 'id']],
            [['lop_truong_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['lop_truong_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'si_so' => 'Si So',
            'lop_truong_id' => 'Lop Truong ID',
            'khoa_id' => 'Khoa ID',
        ];
    }

    /**
     * Gets query for [[Khoa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhoa()
    {
        return $this->hasOne(Khoa::className(), ['id' => 'khoa_id']);
    }

    /**
     * Gets query for [[LopTruong]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLopTruong()
    {
        return $this->hasOne(User::className(), ['id' => 'lop_truong_id']);
    }
}
