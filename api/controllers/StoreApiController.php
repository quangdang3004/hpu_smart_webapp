<?php

namespace api\controllers;

use backend\models\DanhMuc;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\Cookie;
use yii\web\HttpException;

class StoreApiController extends CoreApiController
{


    /** logout */
    public function actionLogout(){
        $cookies = \Yii::$app->response->cookies;
        $cookies->remove('token');
        $cookies->remove('username');
        $cookies->remove('userId');
        return [
            'message' => 'Success',
            'status' => 200,
            'content' => 'Đã đăng xuất thành công'
        ];
    }
}
