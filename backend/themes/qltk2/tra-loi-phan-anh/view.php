<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TraLoiPhanAnh */
?>
<div class="tra-loi-phan-anh-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'noi_dung:ntext',
            'created',
            'updated',
            'file_dinh_kem',
            'hinh_anh',
            'phan_anh_hien_truong_id',
            'user_id',
        ],
    ]) ?>

</div>
