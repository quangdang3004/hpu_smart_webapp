<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PhongBan;

/**
 * PhongBanSearch represents the model behind the search form about `backend\models\PhongBan`.
 */
class PhongBanSearch extends PhongBan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'truong_phong_id'], 'integer'],
            [['ten_phong_ban', 'active'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PhongBan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'truong_phong_id' => $this->truong_phong_id,
        ]);

        $query->andFilterWhere(['like', 'ten_phong_ban', $this->ten_phong_ban])
            ->andFilterWhere(['like', 'active', $this->active]);

        return $dataProvider;
    }
}
