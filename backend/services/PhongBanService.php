<?php

namespace backend\services;

use backend\helpers\ConstHelper;
use backend\models\ChiTietPhongBanNhanVien;
use backend\models\DanhMuc;
use backend\models\PhongBan;
use backend\models\PhongBanNhanVien;
use common\models\User;
use yii\db\ActiveRecord;

class PhongBanService
{
    /**
     * Lấy tất cả phòng ban
     * @param $field
     * @return PhongBan[]
     */
    public static function getAllPhongBan($field = "*"){
        return PhongBan::find()->select($field)->andFilterWhere(['active' => 1])->all();
    }

    /**
     * Lấy tất cả phòng ban select2
     * @return array[]
     */
    public static function getAllPhongBanSelect2(){
//        $phongBan = PhongBan::find()->select(['id as category_id', 'ten_phong_ban as category_name'])->andFilterWhere(['active' => 1])->all();
        $phongBan = \Yii::$app->db->createCommand("Select `id` AS `category_id`, `ten_phong_ban` AS `category_name` from vq_phong_ban where active = 1")->queryAll();
        return [
            'responseBody' => $phongBan,
            'responseTotalResult' => count($phongBan)
        ];
    }
}