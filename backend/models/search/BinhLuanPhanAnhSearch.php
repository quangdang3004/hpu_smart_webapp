<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BinhLuanPhanAnh;

/**
 * BinhLuanPhanAnhSearch represents the model behind the search form about `backend\models\BinhLuanPhanAnh`.
 */
class BinhLuanPhanAnhSearch extends BinhLuanPhanAnh
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'phan_anh_hien_truong_id'], 'integer'],
            [['noi_dung', 'created', 'hinh_anh'], 'safe'],
            [['active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BinhLuanPhanAnh::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created' => $this->created,
            'active' => $this->active,
            'phan_anh_hien_truong_id' => $this->phan_anh_hien_truong_id,
        ]);

        $query->andFilterWhere(['like', 'noi_dung', $this->noi_dung])
            ->andFilterWhere(['like', 'hinh_anh', $this->hinh_anh]);

        return $dataProvider;
    }
}
