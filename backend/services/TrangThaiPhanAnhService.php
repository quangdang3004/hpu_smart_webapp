<?php

namespace backend\services;

use backend\models\PhanAnhHienTruong;
use backend\models\TrangThaiPhanAnhHienTruong;

class TrangThaiPhanAnhService
{
    public static function createTrangThaiPhanAnhWithTrangThaiAndPhanAnhID($trangThai, $phanAnhId){
        $trangThaiPhanAnh = new TrangThaiPhanAnhHienTruong();
        $trangThaiPhanAnh->trang_thai = $trangThai;
        $trangThaiPhanAnh->vq_phan_anh_hien_truong_id = $phanAnhId;
        $trangThaiPhanAnh->created = date('Y-m-d H:i:s');
        if ($trangThaiPhanAnh->save()){
            $phanAnh = PhanAnhHienTruong::findOne($phanAnhId);
            $phanAnh->updateAttributes(['trang_thai' => $trangThai]);
            return true;
        }
        return false;
    }
}