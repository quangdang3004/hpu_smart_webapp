<?php

namespace api\controllers;

use backend\helpers\ConstHelper;
use backend\models\AnhPhanAnhHienTruong;
use backend\models\BinhLuanPhanAnh;
use backend\models\Khoa;
use backend\models\Lop;
use backend\models\QuanLyPhanAnhHienTruong;
use BaconQrCode\Renderer\Text\Html;
use common\models\User;

class BinhLuanPhanAnhController extends AndinCoreApiController
{
    public function actionGuiBinhLuanPhanAnh(){
        $binhLuan = new BinhLuanPhanAnh();
        if ($this->checkNull('uid')){
            $binhLuan->user_id = $_POST['uid'];
        }
        if ($this->checkNull('noiDung')){
            $binhLuan->noi_dung = $_POST['noiDung'];
        }
        if ($this->checkNull('idPhanAnh')){
            $binhLuan->phan_anh_hien_truong_id = $_POST['idPhanAnh'];
        }

        if ($binhLuan->save()){
            $binhLuan->created = date('Y/m/d H:i:s', strtotime($binhLuan->created));
            $binhLuan->user_id = $binhLuan->user;
            $binhLuan->active = $binhLuan->active == 1;
            $binhLuan->phan_anh_hien_truong_id = (int)$binhLuan->phan_anh_hien_truong_id;
            return [
                'message' => 'Gửi bình luận thành công',
                'binhLuanPhanAnh' => $binhLuan,
            ];
        }
        return $this->error500(\kartik\helpers\Html::errorSummary($binhLuan));
    }
}