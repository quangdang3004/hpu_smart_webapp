<?php

namespace api\controllers;

use backend\helpers\ConstHelper;
use backend\models\AnhPhanAnhHienTruong;
use backend\models\BinhLuanPhanAnh;
use backend\models\DanhMuc;
use backend\models\Khoa;
use backend\models\Lop;
use backend\models\PhanAnhHienTruong;
use backend\models\QuanLyPhanAnhHienTruong;
use backend\models\TraLoiPhanAnh;
use backend\services\DanhMucService;
use backend\services\PhongBanService;
use BaconQrCode\Renderer\Text\Html;
use common\models\User;
use yii\helpers\VarDumper;

class PhanAnhHienTruongController extends AndinCoreApiController
{
    public function actionDuLieuDropDown()
    {
        $khuVuc = DanhMucService::getDanhMucSelect2(DanhMuc::KHU_VUC);
        $mucDo = DanhMucService::getDanhMucSelect2(DanhMuc::MUC_DO_PHAN_ANH);
        $chuyenMuc = DanhMucService::getDanhMucSelect2(DanhMuc::CHUYEN_MUC);
        $phongBan = PhongBanService::getAllPhongBanSelect2();

        return [
            'khuVuc' => $khuVuc,
            'phongBan' => $phongBan,
            'mucDo' => $mucDo,
            'chuyenMuc' => $chuyenMuc
        ];
    }

    public function actionGuiPhanAnh()
    {
        $phanAnhHienTruong = new PhanAnhHienTruong();
        if ($this->checkNull('phongBanId')) {
            $phanAnhHienTruong->phong_ban_id = $_POST['phongBanId'];
        }
        if ($this->checkNull('mucDoId')) {
            $phanAnhHienTruong->muc_do_phan_anh_id = $_POST['mucDoId'];
        }
        if ($this->checkNull('khuVucId')) {
            $phanAnhHienTruong->khu_vuc_id = $_POST['khuVucId'];
        }
        if ($this->checkNull('chuyenMucId')) {
            $phanAnhHienTruong->chuyen_muc_id = $_POST['chuyenMucId'];
        }
        if ($this->checkNull('noiDung')) {
            $phanAnhHienTruong->noi_dung = $_POST['noiDung'];
        }

        if ($this->checkNull('uid')) {
            $phanAnhHienTruong->user_id = $_POST['uid'];
        }

        if ($phanAnhHienTruong->save()) {

            if ($this->checkNull('anhPhanAnh')) {

                $anhPhanAnh = explode(',',str_replace(['[', ']', ' '],'',$_POST['anhPhanAnh']));
                foreach ($anhPhanAnh as $index => $item) {
                    $anh = new AnhPhanAnhHienTruong();
                    $dataImage = explode('|',$item);
                    if(count($dataImage) == 2){
                        $image = base64_decode(str_replace('PLUSICON', '+', str_replace('RIGHTDASH', '/', $dataImage[1])));
                        $link = '/images/' . rand(1, time()) . $index .'.'. $dataImage[0];
                        $anh->image = $link;
                        file_put_contents(\Yii::getAlias('@root') . $link, $image);
                        $anh->phan_anh_id = $phanAnhHienTruong->id;
                        if (!$anh->save()){
                            return $this->error500(\kartik\helpers\Html::errorSummary($anh));
                        }
                    }

                }
                return [
                    'message' => 'Gửi phản ánh thành công',
                    'phanAnhId' => $phanAnhHienTruong->id
                ];
            }
        } else {
            return $this->error500(\kartik\helpers\Html::errorSummary($phanAnhHienTruong));
        }
    }

    public function actionChiTietPhanAnhHienTruong(){
        $idPhanAnh = $_POST['idPhanAnh'];
        $phanAnh = QuanLyPhanAnhHienTruong::findOne(['id' => $idPhanAnh]);
        $anhPhanAnh = AnhPhanAnhHienTruong::findAll(['phan_anh_id' => $idPhanAnh]);
        $binhLuanPhanAnh = BinhLuanPhanAnh::find()->andWhere(['phan_anh_hien_truong_id' => $idPhanAnh])->orderBy(['created' => SORT_DESC])->all();
        $traLoiPhanAnh = TraLoiPhanAnh::findOne(['phan_anh_hien_truong_id' => $idPhanAnh]);
        $anhTraLoiPhanAnh = null;
        if (!is_null($traLoiPhanAnh)){
            $anhTraLoiPhanAnh = AnhPhanAnhHienTruong::findAll(['tra_loi_phan_anh_id' => $traLoiPhanAnh->id]);
        }
        foreach ($binhLuanPhanAnh as $item){
            $item->created = date('Y/m/d H:i:s', strtotime($item->created));
            $item->user_id = $item->user;
        }

        if (isset($anhPhanAnh)){
            foreach ($anhPhanAnh as $item){
                $item->image = ConstHelper::BASE_URL.$item->image;
            }
        }

        if (isset($anhTraLoiPhanAnh)){
            foreach ($anhTraLoiPhanAnh as $item){
                $item->image = ConstHelper::BASE_URL.$item->image;
            }
        }


        $phanAnh->created = date('Y/m/d H:i:s', strtotime($phanAnh->created));

        if (!is_null($traLoiPhanAnh)){
            $hoTen = User::findOne(['id' => $traLoiPhanAnh->user_id]);

            $traLoiPhanAnh->created = date('Y/m/d H:i:s', strtotime($traLoiPhanAnh->created));
            $traLoiPhanAnh->user_id = $hoTen->ho_ten;
        }

        if ($phanAnh->user_id != null){
            $userPhanAnh = User::findOne([$phanAnh->user_id]);
            $lop = null;
            $khoa = null;
            if (!is_null($userPhanAnh->lop_id)){
                $lop = Lop::findOne(['id' => $userPhanAnh->lop_id]);
                if (!is_null($lop->khoa_id)){
                    $khoa = Khoa::findOne(['id' => $lop->khoa_id]);
                }
            }
            return [
                'phanAnhHienTruong' => $phanAnh,
                'anhPhanAnh' => $anhPhanAnh,
                'userPhanAnh' => $userPhanAnh,
                'lop' => !is_null($lop) ? $lop->name : null,
                'khoa' => !is_null($lop) ? $khoa->name : null,
                'traLoiPhanAnh' => $traLoiPhanAnh,
                'binhLuanPhanAnh' => $binhLuanPhanAnh,
                'anhTraLoiPhanAnh' => $anhTraLoiPhanAnh,
            ];
        }
        return [
            'phanAnhHienTruong' => $phanAnh,
            'anhPhanAnh' => $anhPhanAnh,
            'traLoiPhanAnh' => $traLoiPhanAnh,
            'binhLuanPhanAnh' => $binhLuanPhanAnh,
            'anhTraLoiPhanAnh' => $anhTraLoiPhanAnh,
        ];
    }
}