<?php

namespace backend\models\search;

use backend\models\UserVaiTro;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "vq_user_vai_tro".
 *
 * @property int $id
 * @property string|null $ma_sinh_vien
 * @property string $username Username
 * @property string $password_hash Mật khẩu mã hóa
 * @property string $auth_key Key xác thực người dùng
 * @property string|null $ho_ten Họ tên người dùng
 * @property string|null $dien_thoai Số điện thoại
 * @property string|null $email Email
 * @property string|null $dia_chi Địa chỉ người dùng
 * @property string|null $anh_dai_dien Ảnh đại diện
 * @property int $status 0 là không hoạt động, 10 là hoạt động
 * @property string $created Ngày tạo tài khoản
 * @property string|null $updated Ngày cập nhật tài khoản
 * @property int|null $lop_id
 * @property string|null $ngay_sinh
 * @property string|null $hoten
 * @property int|null $vai_tro_id
 * @property string|null $ten_vai_tro Tên vai trò
 * @property int $count_noti
 * @property string|null $ten_lop
 * @property string|null $ten_khoa
 */
class UserVaiTroSearch extends UserVaiTro
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_vai_tro}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'username', 'password_hash', 'auth_key'], 'required'],
            [['id', 'status', 'lop_id', 'vai_tro_id', 'count_noti'], 'integer'],
            [['created', 'updated', 'ngay_sinh'], 'safe'],
            [['ma_sinh_vien', 'username', 'hoten', 'ten_vai_tro'], 'string', 'max' => 50],
            [['password_hash', 'auth_key', 'email', 'dia_chi', 'ten_lop', 'ten_khoa'], 'string', 'max' => 100],
            [['ho_ten'], 'string', 'max' => 60],
            [['dien_thoai'], 'string', 'max' => 10],
            [['anh_dai_dien'], 'string', 'max' => 45],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserVaiTro::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

//        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    public function beforeValidate()
    {
    }
}
