<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TrangThaiPhanAnhHienTruong;

/**
 * TrangThaiPhanAnhHienTruongSearch represents the model behind the search form about `backend\models\TrangThaiPhanAnhHienTruong`.
 */
class TrangThaiPhanAnhHienTruongSearch extends TrangThaiPhanAnhHienTruong
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'vq_phan_anh_hien_truong_id'], 'integer'],
            [['trang_thai', 'created', 'dete'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrangThaiPhanAnhHienTruong::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created' => $this->created,
            'vq_phan_anh_hien_truong_id' => $this->vq_phan_anh_hien_truong_id,
        ]);

        $query->andFilterWhere(['like', 'trang_thai', $this->trang_thai])
            ->andFilterWhere(['like', 'dete', $this->dete]);

        return $dataProvider;
    }
}
