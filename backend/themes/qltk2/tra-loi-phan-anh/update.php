<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TraLoiPhanAnh */
?>
<div class="tra-loi-phan-anh-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
