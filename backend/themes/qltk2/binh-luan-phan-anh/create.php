<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\BinhLuanPhanAnh */

?>
<div class="binh-luan-phan-anh-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
