<?php
return [
    'language'=>'vi',
    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=hpu_smart',
            'username' => 'root',
            'password' => '123456',
            'attributes' => [PDO::ATTR_CASE => PDO::CASE_LOWER],
            'charset' => 'utf8',
            'tablePrefix' => 'vq_'
        ],
    ],
];