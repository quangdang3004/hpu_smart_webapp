<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TrangThaiPhanAnhHienTruong */
?>
<div class="trang-thai-phan-anh-hien-truong-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
