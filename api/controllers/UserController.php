<?php

namespace api\controllers;

use api\services\UserServices;

class UserController extends AndinCoreApiController
{
    public function actionDanhSachUser(){
        $user = UserServices::getUserByUidAuth($_POST['uid'], $_POST['auth']);
        if (!isset($user))
            $this->error500('Không tìm thấy người dùng');
        $listUser = UserServices::getAllUser();
        return [
            'listUser' => $listUser
        ];
    }
}