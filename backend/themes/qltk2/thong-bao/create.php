<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ThongBao */

?>
<div class="thong-bao-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
