<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TrangThaiPhanAnhHienTruong */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trang-thai-phan-anh-hien-truong-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'trang_thai')->dropDownList([ 'Chờ duyệt' => 'Chờ duyệt', 'Duyệt' => 'Duyệt', 'Hủy' => 'Hủy', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'dete')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vq_phan_anh_hien_truong_id')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
