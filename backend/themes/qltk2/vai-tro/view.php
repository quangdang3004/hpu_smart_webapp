<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\VaiTro */
?>
<div class="vai-tro-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ten_vai_tro',
        ],
    ]) ?>

</div>
