<?php

namespace api\controllers;

use api\models\ChamCong;

class ChamCongController extends AndinCoreApiController
{
    public function actionGetChamCong(){
        $nam = date('Y');
        $thang = date('m');
        if(isset($_POST['thang'])){
            if(is_numeric($_POST['thang']) && $_POST['thang'] != "")
                $thang = $_POST['thang'];

        }
        if(isset($_POST['nam'])){
            if(is_numeric($_POST['nam']) && $_POST['nam'] != "")
                $nam = $_POST['nam'];
        }
        $data_cham_cong = ChamCong::find()
            ->andFilterWhere(['month(date)'=>$thang,'year(date)'=>$nam])
            ->andFilterWhere(['nhan_vien_id'=>$_POST['uid']])
            ->orderBy(['date(date)'=>SORT_ASC])
            ->all();
        $data = null;
        foreach ($data_cham_cong as $item){
            /**
             * @var $item ChamCong
             */
            $data[$item->date]['check_in_morning'] = $this->datetime2Hi($item->vao1);
            $data[$item->date]['check_out_morning'] =  $this->datetime2Hi($item->ra1);
            $data[$item->date]['check_in_afternoon'] =  $this->datetime2Hi($item->vao2);
            $data[$item->date]['check_out_afternoon'] =  $this->datetime2Hi($item->ra2);
            $data[$item->date]['type'] = $item->trang_thai;
            $data[$item->date]['note'] = $item->ghi_chu;
        }
        return ['data'=>$data];
    }
}