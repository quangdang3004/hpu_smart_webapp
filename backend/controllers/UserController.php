<?php

namespace backend\controllers;

use backend\helpers\ConstHelper;
use backend\models\CauHinh;
use backend\models\DanhMuc;
use backend\models\search\UserVaiTroSearch;
use backend\models\ThongBao;
use backend\models\Vaitrouser;
use common\models\myAPI;
use common\models\User;
use Symfony\Component\Translation\Dumper\PoFileDumper;
use backend\services\UserService;
use Yii;
use yii\base\Security;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use function GuzzleHttp\Promise\all;

class UserController extends CoreController
{
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        $arr_action = ['index', 'create', 'update', 'delete', 'view', 'thong-tin-ca-nhan',
            'load-form-ho-so-ca-nhan', 'luu-thong-tin-ca-nhan', 'xoa'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name = strtolower(str_replace('action', '', $action->id));
                    return myAPI::isAccess2('User', $action_name);
                }
            ];
        }

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /** index */
    public function actionIndex()
    {
        $searchModel = new UserVaiTroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /** view */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Thông tin thành viên",
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Đóng', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Chỉnh sửa', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /** create */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new User();
        $vaitros = [];
        $vaitrouser = new Vaitrouser();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Thêm thành viên mới",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser,
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post())) {
                if ($model->username == '') {
                    return [
                        'title' => "Thêm thành viên mới",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'vaitros' => $vaitros,
                            'vaitrouser' => $vaitrouser
                        ]),
                        'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                } else {
                    $model->setPassword($model->password_hash);
                    if ($model->save())
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => "Thêm thành viên mới",
                            'content' => '<span class="text-success">Thêm mới thành viên thành công</span>',
                            'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::a('<i class="glyphicon glyphicon-plus"></i> Tạo thêm', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                        ];
                    else
                        return [
                            'title' => Html::errorSummary($model),
                            'content' => $this->renderAjax('create', [
                                'model' => $model,
                                'vaitros' => $vaitros,
                                'vaitrouser' => $vaitrouser
                            ]),
                            'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                        ];
                }
            } else {
                return [
                    'title' => "Thêm thành viên mới",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /** update */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = UserService::getUserById($id);
        $vaitros = ArrayHelper::map(Vaitrouser::findAll(['user_id' => $id]), 'vai_tro_id', 'vai_tro_id');
        $vaitrouser = new Vaitrouser();
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Chỉnh sửa thông tin thành viên",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser,

                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post())) {
                $oldUser = User::findOne($id);

                if ($oldUser->password_hash != $model->password_hash)
                    $model->setPassword($model->password_hash);
                if ($model->id == 1) {
                    if (Yii::$app->user->id != 1) {
                        echo Json::encode(['message' => myAPI::getMessage('danger', 'Bạn không có quyền thực hiện chức năng này')]);
                        exit;
                    }
                }

                if ($model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Thông tin thành viên",
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('<i class="fa fa-edit"></i> Chỉnh sửa', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Cập nhật thông tin thành viên " . $model->hoten,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                            'vaitros' => $vaitros,
                            'vaitrouser' => $vaitrouser
                        ]),
                        'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            } else {
                return [
                    'title' => "Chỉnh sửa thông tin thành viên",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /** delete */
    public function actionDelete($id)
    {
        if ($id != 1 || ($id == 1 && Yii::$app->user->id == 1)) {
            $request = Yii::$app->request;
            UserService::deleteUserById($id);

            if ($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            } else {
                return $this->redirect(['index']);
            }
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }
    }

    /** bulk-delete */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks'));
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            return $this->redirect(['index']);
        }

    }

    /** thong-tin-ca-nhan */
    public function actionThongTinCaNhan()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => $this->renderAjax('form-cap-nhat')
        ];
    }

    /** load-form-ho-so-ca-nhan */
    public function actionLoadFormHoSoCaNhan()
    {
        if (!is_null(\Yii::$app->user)) {
            $title = "Hồ sơ cá nhân";
            $model = User::findOne(['id' => \Yii::$app->user->id]);
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post()) && $model->save()) {
                return [
                    'title' => "Cập nhật hồ sơ",
                    'content' => '<span class="text-success">Cập nhật hồ sơ thành công </span>',
                    'footer' => Html::button(ConstHelper::BUTTON_LABEL_CLOSE, ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Cập nhật', ['user/load-form-ho-so-ca-nhan'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            }
            return [
                'content' => $this->renderAjax('form-cap-nhat', [
                    'model' => $model,
                ]),
                'title' => $title,
                'footer' => Html::button(ConstHelper::BUTTON_LABEL_CLOSE, ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(ConstHelper::BUTTON_LABEL_SAVE, ['class' => 'btn btn-primary', 'type' => "submit"]),
                'size' => 'large'
            ];
        }
    }
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
