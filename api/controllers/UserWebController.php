<?php

namespace api\controllers;

use common\models\User;
use yii\rest\ActiveController;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\HttpException;
use yii\web\Response;

class UserWebController extends ActiveController
{
    public $modelClass = "common\models\User";

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function verbs()
    {
        return [
            'login' => ['POST'],
        ];
    }

    /** login */
    public function actionLogin(){
        if(!isset($_POST['username']))
            throw new HttpException(500, 'Không có thông tin tên đăng nhập');
        else if(!isset($_POST['password']))
            throw new HttpException(500, 'Không có thông tin mật khẩu');
        else{
            if(empty($_POST['username']) || empty($_POST['password'])){
                throw new HttpException(500, 'Tên đăng nhập hoặc số điện thoại và mật khẩu không được để trống');
            }else{
                /** @var User $user */
                $user = User::find()
                    ->andWhere('username = :u or dien_thoai = :u', [
                        ':u' => $_POST['username']
                    ])
                    ->andFilterWhere(['status' => 10])
                    ->one();
                if(is_null($user)){
                    throw new HttpException(500, 'Tên đăng nhập hoặc số điện thoại không đúng');
                }else{
                    if(\Yii::$app->security->validatePassword($_POST['password'], $user->password_hash)){
                        $auth = \Yii::$app->security->generateRandomString(32);
                        $user->updateAttributes(['auth_key' => $auth]);

//                        $cookies = \Yii::$app->response->cookies;
//                        $cookies->add(new Cookie([
//                            'name' => 'token',
//                            'value' => $auth
//                        ]));
//                        $cookies->add(new Cookie([
//                            'name' => 'username',
//                            'value' => \Yii::$app->security->generateRandomString()
//                        ]));
//                        $cookies->add(new Cookie([
//                            'name' => 'userId',
//                            'value' => $user->username
//                        ]));

                        return [
                            'message' => 'Đăng nhập thành công',
                            'uid' => $user->id,
                            'auth' => $auth,
                            'user' => User::findOne($user->id),
                        ];
                    }
                    else
                        throw new HttpException(500, 'Mật khẩu không đúng');
                }
            }
        }
    }
}