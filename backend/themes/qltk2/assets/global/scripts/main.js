 /**
 * Created by HungLuongHien on 6/23/2016.
 */
function getMessage(str) {
    return str.replace('Internal Server Error (#500):','');
}
function createTypeHead(target, action, callbackAfterSelect){
    $(target).typeahead({
        source: function (query, process) {
            var states = [];
            return $.get('index.php?r=autocomplete/'+action, { query: query }, function (data) {
                $.each(data, function (i, state) {
                    states.push(state.name);
                });
                return process(states);
            }, 'json');
        },
        afterSelect: function (item) {
            if(typeof callbackAfterSelect != 'undefined')
                callbackAfterSelect(item);
            /*$.ajax({
             url: 'index.php?r=khachhang/getdiachi',
             data: {name: item},
             type: 'post',1
             dataType: 'json',
             success: function (data) {
             $("#diachikhachhang").val(data);
             }
             })*/
        }
    });
}
function setDatePicker() {
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'vi',
        todayBtn: true,
        todayHighlight: true,
    });
}
function uniqId() {
    return Math.round(new Date().getTime() + (Math.random() * 100));
}

function SaveObjectUploadFile($url_controller_action, $dataInput, callbackSuccess, columnClass){
    if(typeof columnClass == "undefined")
        columnClass = 's';
    // data = new FormData($(modalForm)[0]);
    $.dialog({
        columnClass: columnClass,
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=' + $url_controller_action,
                type: 'post',
                dataType: 'json',
                data: $dataInput,
                // async: false,
                contentType: false,
                // cache: false,
                processData: false
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                return false;
            });
        }
    })
}


function SaveObject($url_controller_action, $dataInput, callbackSuccess, columnClass){
    if(typeof columnClass == "undefined")
        columnClass = 's';

    $.dialog({
        columnClass: columnClass,
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=' + $url_controller_action,
                type: 'post',
                dataType: 'json',
                data: $dataInput,
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                return false;
            });
        }
    })
}
/*
@param
  actionloadform  = action xử lý
  dataloadform ={} dữ liệu truyền vào khi open form
  những button trong form được truyền vào biến obj_button với đddinh dạng
  obj_button = {tên button:{
                    class:'class button',
                    text:'text của nút',
                    action:sự kiện action gọi ajax nếu co thì true,
                    url:'đường dẫ gọi ajax khi nhân button',
                    data:{data truền vào ajax
                    },
                    mess:'alert ra một dòng chữ khi thành công'
                },... những button khac

  }
* */
function loadForm(actionloadform, dataloadform={},$size = 'm', callbackSuccess, callbackSave,obj_button = {}) {
    const button ={};
    for (const [key, value] of Object.entries(obj_button)) {
           button[key] = {};
           button[key]['text'] = value['text'];
            button[key]['btnClass'] = value['class'];
            if(value['ajax'] === true )
            {
                button[key]['action'] = function () {
                    console.log(value['data'])
                    $.ajax({
                        type: "POST",
                        url: actionloadform,
                        data: value['data'],
                        dataType: 'json',
                        beforeSend: function (){

                        },
                        success: function (response){
                            alertify.success(value['mess']);
                            $.pjax.reload({container: "#crud-datatable-pjax"});

                        },
                        error: function(r1, r2){
                            alertify.error(r1.responseText);
                        }
                    })
                };

            }
    }
    button['btnClose'] =     {
        text: '<i class="fa fa-close"></i> Đóng lại'
    };
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: actionloadform,
                data: dataloadform,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: button
    });
}
//  function loadForm($dataInput, $size = 'm', callbackSuccess, callbackSave) {
//      $.confirm({
//          content: function () {
//              var self = this;
//              return $.ajax({
//                  url: 'index.php?r=site/loadform',
//                  data: $dataInput,
//                  type: 'post',
//                  dataType: 'json'
//              }).success(function (data) {
//                  self.setContent(data.content);
//                  self.setTitle(data.title);
//                  self.setType('blue');
//                  callbackSuccess(data);
//              }).error(function (r1, r2) {
//                  self.setContent(getMessage(r1.responseText));
//                  self.setTitle('Lỗi');
//                  self.setType('red');
//                  self.$$btnSave.prop('disabled', true);
//                  return false;
//              });
//          },
//          columnClass: $size,
//          buttons: {
//              btnSave: {
//                  text: '<i class="fa fa-check-circle"> Duyệt</i>',
//                  btnClass: 'btn btn-primary',
//                  action: function () {
//                      $.ajax({
//                          type: "POST",
//                          url: 'index.php?r=tuyen-dung-dk-nhu-cau-ns/cap-nhat-trang-thai-phieu',
//                          data: {
//                              id: $dataInput['id'],
//                              trangThai: 'Đã duyệt'
//                          },
//                          dataType: 'json',
//                          beforeSend: function (){
//
//                          },
//                          success: function (response){
//                              alertify.success('Cập nhật trạng thái phiếu dụng thành công');
//                              $.pjax.reload({container: "#crud-datatable-pjax"});
//                          },
//                          error: function(r1, r2){
//                              console.log(r1.responseText)
//                          }
//                      })
//                  }
//              },
//              somethingElse: {
//                  text: '<i class="fa fa-ban"> Không duyệt</i>',
//                  btnClass: 'btn btn-danger',
//                  action: function () {
//                      $.ajax({
//                          type: "POST",
//                          url: 'index.php?r=tuyen-dung-dk-nhu-cau-ns/cap-nhat-trang-thai-phieu',
//                          data: {
//                              id: $dataInput['id'],
//                              trangThai: 'Không duyệt'
//                          },
//                          dataType: 'json',
//                          beforeSend: function (){
//
//                          },
//                          success: function (response){
//                              alertify.success('Cập nhật trạng thái phiếu dụng thành công');
//                              $.pjax.reload({container: "#crud-datatable-pjax"});
//                          },
//                          error: function(r1, r2){
//                              console.log(r1.responseText)
//                          }
//                      })
//                  }
//              },
//              btnClose: {
//                  text: '<i class="fa fa-close"></i> Đóng lại'
//              }
//          }
//      });
//  }
 function loadForm2($dataInput, $size = 'm', callbackSuccess, callbackSave, textBtnAccept = '<i class="fa fa-save"></i> Lưu lại') {

     $.confirm({

         content: function () {

             var self = this;

             return $.ajax({

                 url: 'index.php?r=site/loadform',

                 data: $dataInput,

                 type: 'post',

                 dataType: 'json'

             }).success(function (data) {

                 self.setContent(data.content);

                 self.setTitle(data.title);

                 self.setType('blue');

                 callbackSuccess(data);

             }).error(function (r1, r2) {

                 self.setContent(getMessage(r1.responseText));

                 self.setTitle('Lỗi');

                 self.setType('red');

                 self.$$btnSave.prop('disabled', true);

                 return false;

             });

         },

         columnClass: $size,

         buttons: {

             btnSave: {

                 text: textBtnAccept,

                 btnClass: 'btn-primary',

                 action: function () {

                     if(typeof callbackSave != "undefined") return callbackSave();

                 }

             },

             btnClose: {

                 text: '<i class="fa fa-close"></i> Đóng lại'

             }

         }

     });

 }

 function loadFormNguoiPhuTrach($dataInput, $size = 'm', callbackSuccess, callbackSave) {
     $.confirm({
         content: function () {
             var self = this;
             return $.ajax({
                 url: 'index.php?r=site/loadformnguoiphutrach',
                 data: $dataInput,
                 type: 'post',
                 dataType: 'json'
             }).success(function (data) {
                 self.setContent(data.content);
                 self.setTitle(data.title);
                 self.setType('blue');
                 callbackSuccess(data);
             }).error(function (r1, r2) {
                 self.setContent(getMessage(r1.responseText));
                 self.setTitle('Lỗi');
                 self.setType('red');
                 self.$$btnSave.prop('disabled', true);
                 return false;
             });
         },
         columnClass: $size,
         buttons: {
             btnSave: {
                 text: '<i class="fa fa-save"></i> Lưu lại',
                 btnClass: 'btn-primary',
                 action: function () {
                     if(typeof callbackSave != "undefined") return callbackSave();
                 }
             },
             btnClose: {
                 text: '<i class="fa fa-close"></i> Đóng lại'
             }
         }
     });
 }

 function loadFormTrangThai($dataInput, $size = 'm', callbackSuccess, callbackSave) {
     $.confirm({
         content: function () {
             var self = this;
             return $.ajax({
                 url: 'index.php?r=site/loadformtrangthai',
                 data: $dataInput,
                 type: 'post',
                 dataType: 'json'
             }).success(function (data) {
                 self.setContent(data.content);
                 self.setTitle(data.title);
                 self.setType('blue');
                 callbackSuccess(data);
             }).error(function (r1, r2) {
                 self.setContent(getMessage(r1.responseText));
                 self.setTitle('Lỗi');
                 self.setType('red');
                 self.$$btnSave.prop('disabled', true);
                 return false;
             });
         },
         columnClass: $size,
         buttons: {
             btnSave: {
                 text: '<i class="fa fa-save"></i> Lưu lại',
                 btnClass: 'btn-primary',
                 action: function () {
                     if(typeof callbackSave != "undefined") return callbackSave();
                 }
             },
             btnClose: {
                 text: '<i class="fa fa-close"></i> Đóng lại'
             }
         }
     });
 }

 function loadFormThuPhi($dataInput, $size = 'm', callbackSuccess, callbackSave) {
     $.confirm({
         content: function () {
             var self = this;
             return $.ajax({
                 url: 'index.php?r=site/loadformthuphi',
                 data: $dataInput,
                 type: 'post',
                 dataType: 'json'
             }).success(function (data) {
                 self.setContent(data.content);
                 self.setTitle(data.title);
                 self.setType('blue');
                 callbackSuccess(data);
             }).error(function (r1, r2) {
                 self.setContent(getMessage(r1.responseText));
                 self.setTitle('Lỗi');
                 self.setType('red');
                 self.$$btnSave.prop('disabled', true);
                 return false;
             });
         },
         columnClass: $size,
         buttons: {
             btnSave: {
                 text: '<i class="fa fa-save"></i> Lưu lại',
                 btnClass: 'btn-primary',
                 action: function () {
                     if(typeof callbackSave != "undefined") return callbackSave();
                 }
             },
             btnClose: {
                 text: '<i class="fa fa-close"></i> Đóng lại'
             }
         }
     });
 }
 function loadFormFromUrl($dataInput, $controller_action, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=' + $controller_action,
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnSave: {
                text: '<i class="fa fa-save"></i> Lưu lại',
                btnClass: 'btn-primary',
                action: function () {
                    if(typeof callbackSave != "undefined") return callbackSave();
                }
            },
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}
function loadFormThayTheNguoiPhuTrach($dataInput, $size = 'm', callbackSuccess, callbackSave) {
     $.confirm({
         content: function () {
             var self = this;
             return $.ajax({
                 url: 'index.php?r=site/loadformthaythenguoiphutrach',
                 data: $dataInput,
                 type: 'post',
                 dataType: 'json'
             }).success(function (data) {
                 self.setContent(data.content);
                 self.setTitle(data.title);
                 self.setType('blue');
                 callbackSuccess(data);
             }).error(function (r1, r2) {
                 self.setContent(getMessage(r1.responseText));
                 self.setTitle('Lỗi');
                 self.setType('red');
                 self.$$btnSave.prop('disabled', true);
                 return false;
             });
         },
         columnClass: $size,
         buttons: {
             btnSave: {
                 text: '<i class="fa fa-save"></i> Lưu lại',
                 btnClass: 'btn-primary',
                 action: function () {
                     if(typeof callbackSave != "undefined") return callbackSave();
                 }
             },
             btnClose: {
                 text: '<i class="fa fa-close"></i> Đóng lại'
             }
         }
     });
 }
 function loadFormKinhDoanhHoiVien($dataInput, $size = 'm', callbackSuccess, callbackSave) {
     $.confirm({
         content: function () {
             var self = this;
             return $.ajax({
                 url: 'index.php?r=site/loadformkinhdoanhhoivien',
                 data: $dataInput,
                 type: 'post',
                 dataType: 'json'
             }).success(function (data) {
                 self.setContent(data.content);
                 self.setTitle(data.title);
                 self.setType('blue');
                 callbackSuccess(data);
             }).error(function (r1, r2) {
                 self.setContent(getMessage(r1.responseText));
                 self.setTitle('Lỗi');
                 self.setType('red');
                 self.$$btnSave.prop('disabled', true);
                 return false;
             });
         },
         columnClass: $size,
         buttons: {
             btnSave: {
                 text: '<i class="fa fa-save"></i> Lưu lại',
                 btnClass: 'btn-primary',
                 action: function () {
                     if(typeof callbackSave != "undefined") return callbackSave();
                 }
             },
             btnClose: {
                 text: '<i class="fa fa-close"></i> Đóng lại'
             }
         }
     });
 }
function taiFileExcel($controller_action, $data){
    $.ajax({
        url: 'index.php?r='+$controller_action,
        data: $data,
        dataType: 'json',
        type: 'post',
        beforeSend: function () {
            $('.thongbao').html('');
            Metronic.blockUI();
        },
        success: function (data) {
            $.dialog({
                title: data.title,
                content: data.link_file,
            });
        },
        complete: function () {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            $('.thongbao').html(r1.responseText);
        }
    });
}
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
function viewData($controller_action, $dataInput, $size, callbackSuccess){
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=' + $controller_action,
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                if(typeof callbackSuccess != "undefined")
                    callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}
function showAlert($message){
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": function () {
            // if($link != '')
            //     window.location = $link
        },
        "showDuration": "10000",
        "hideDuration": "1000",
        "timeOut": "10000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr['info']($message, 'Thông báo');
}
function tinhDiemMoiCaNhan($idNhanVienPhongBan){
    var $tongDiem = 0;
    $("tr.tr-nhan-vien-"+$idNhanVienPhongBan).each(function () {
        var $myParentTr = $(this).parent().parent().parent().parent();
        var $myCheckBox = $myParentTr.find('.checkChonCVQuy ');
        if($myCheckBox.is(":checked")){
            var $myInputDiem = $(this).find('td.td-diem-nhan-vien input').val();
            var $thucHien = $(this).find('td.td-thuc-hien select').val();
            $myInputDiem = ($myInputDiem == '' ? 0 : parseFloat($myInputDiem));
            if($thucHien == 1)
                $tongDiem += $myInputDiem;
        }

    });
    $("#diem-nvien-" + $idNhanVienPhongBan).text($tongDiem);
}

 function loadFormModel($dataInput, $size = 'modal-full', $obj, btnSave = () => {}, btnClose = () => {})
 {
     $.ajax({
         url: 'index.php?r=site/load-form-modal',
         data: $dataInput,
         dataType: 'json',
         type: 'post',
         beforeSend: function () {
             $.blockUI();
         },
         success: function (data) {
             $(".modal").remove()
             $(".modal-backdrop").remove()
             $($obj).html(data)
             $(".modal-dialog").addClass($size);
             $("#modal-id").modal('show');
             $($obj + ' .modal-footer .btn-primary').remove();
             jQuery('.date').datepicker($.extend({}, $.datepicker.regional['vi'], {
                 "changeMonth": true,
                 "yearRange": "1972:2022",
                 "changeYear": true,
                 "dateFormat": "dd\/mm\/yy"
             }));

         },
         complete: function () {
             $.unblockUI();
         },
         error: function (r1, r2) {
             $.alert(r1.responseText)
         }
     })

     $(document).on("click", $obj + ' .modal-footer .btn-default', function () {
         btnClose();
     })
 }

function tinhTongDiemDonVi(){
    var $tongDiem = 0;
    $(".diem-so-don-vi input").each(function () {
        var $diemSo = $(this).val();
        $tongDiem += ($diemSo == '' ? 0 : parseFloat($diemSo));
    });
    $("span#tong-diem").text($tongDiem);
}

jQuery(document).ready(function() {

    Metronic.init(); // init metronic core components
    Layout.init(); // init current layout
    QuickSidebar.init(); // init quick sidebar
    // $(".tien-te").maskMoney({thousands:",", allowZero:true, /*suffix: " VNĐ",*/precision:3});
    // Hiển thị thông báo các công việc đã hoàn thành nhưng chưa duyệt
    $(document).on('click', 'ul li a.hover-initialized', function (e) {
        e.preventDefault();

        var $parent = $(this).parent();
        if($parent.find('ul').length > 0){
            $parent.addClass('open');
            $(this).attr('aria-expanded', 'true');
        }
    });

    $('#crud-datatable-pjax').on('pjax:success', function () {
        $('.created-date-field').datepicker({dateFormat: 'mm/dd/yy'})
    })
})

