<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DanhMuc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="danh-muc-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ten_danh_muc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([ 'Khoa' => 'Khoa', 'Lớp' => 'Lớp', 'Khu vực' => 'Khu vực', 'Mức độ phản ánh' => 'Mức độ phản ánh', 'Chuyên mục' => 'Chuyên mục' ], ['prompt' => '']) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')->textInput() ?>

    <?= $form->field($model, 'active')->checkbox() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
