<?php

namespace api\controllers;

use backend\controllers\CoreController;
use backend\services\VaiTroService;
use common\models\myAPI;
use backend\models\VaiTro;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\helpers\Html;

class VaiTroController extends CoreApiController
{
    public function __construct($id, $module, $config = [])
    {
        $this->allowActions[] = 'get-all-vai-tro';
        parent::__construct($id, $module, $config);
    }

    //get-all-vai-tro
    public function actionGetAllVaiTro(){
        return ['optionVaiTro' => VaiTroService::getAllVaiTro()];
    }

    // get-data
    public function actionGetData(){
        $query = VaiTro::find();
        $totalCount = $query->count();
        $data = $query
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->limit($this->dataPost['limit'])
            ->all();

        return [
            'results' => $data,
            'rows' => $totalCount
        ];
    }

    /** save */
    public function actionSave(){
        if($this->dataPost['id'] == '')
            $model = new VaiTro();
        else
            $model = VaiTro::findOne($this->dataPost['id']);
        $model->name = $this->dataPost['name'];
        if($model->save())
            return [
                'content' => 'Đã lưu thông tin vai trò '.$model->ten_vai_tro
            ];
        else
            throw new HttpException(500, Html::errorSummary($model));
    }

    /** load */
    public function actionLoad(){
        $model = VaiTro::findOne($this->dataPost['vai_tro']);
        return [
            'result' => $model
        ];
    }

    /** delete */
    public function actionDelete(){
        $model = VaiTro::findOne($this->dataPost['vai_tro']);
        if($model->delete())
            return [
                'message' => 'Đã xóa dữ liệu vai trò '.$model->ten_vai_tro.' thành công',
            ];
        else
            throw new HttpException(500, Html::errorSummary($model));
    }
}
