<?php

namespace backend\services;

use backend\models\ChucNang;

class ChucNangService
{
    public static function getNhomChucNang(){
        $data = ChucNang::find()
            ->groupBy(['nhom'])
            ->all();
        $arr_data = [];
        /** @var ChucNang $item */
        foreach ($data as $item) {
            $arr_data[] = ['label' => $item->nhom, 'key' => $item->nhom];
        }
        return $arr_data;
    }
}