<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PhongBan */
?>
<div class="phong-ban-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
