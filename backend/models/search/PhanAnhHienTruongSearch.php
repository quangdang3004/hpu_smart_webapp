<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PhanAnhHienTruong;

/**
 * PhanAnhHienTruongSearch represents the model behind the search form about `backend\models\PhanAnhHienTruong`.
 */
class PhanAnhHienTruongSearch extends PhanAnhHienTruong
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['noi_dung', 'hinh_anh', 'file_dinh_kem', 'trang_thai', 'muc_do_phan_anh', 'created', 'updated'], 'safe'],
            [['khan'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PhanAnhHienTruong::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'khan' => $this->khan,
            'created' => $this->created,
            'updated' => $this->updated,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'noi_dung', $this->noi_dung])
            ->andFilterWhere(['like', 'hinh_anh', $this->hinh_anh])
            ->andFilterWhere(['like', 'file_dinh_kem', $this->file_dinh_kem])
            ->andFilterWhere(['like', 'trang_thai', $this->trang_thai])
            ->andFilterWhere(['like', 'muc_do_phan_anh', $this->muc_do_phan_anh]);

        return $dataProvider;
    }
}
