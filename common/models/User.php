<?php

namespace common\models;

use backend\models\Khoa;
use backend\models\Lop;
use backend\models\PhanAnhHienTruong;
use backend\models\PhongBan;
use backend\models\ThongBao;
use backend\models\Vaitrouser;
use Yii;
use yii\base\NotSupportedException;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "vq_user".
 *
 * @property int $id
 * @property string|null $ma_sinh_vien
 * @property string $username Username
 * @property string $password_hash Mật khẩu mã hóa
 * @property string $auth_key Key xác thực người dùng
 * @property string|null $ho_ten Họ tên người dùng
 * @property string|null $dien_thoai Số điện thoại
 * @property string|null $email Email
 * @property string|null $dia_chi Địa chỉ người dùng
 * @property string|null $anh_dai_dien Ảnh đại diện
 * @property int $status 0 là không hoạt động, 10 là hoạt động
 * @property string $created Ngày tạo tài khoản
 * @property string|null $updated Ngày cập nhật tài khoản 
 * @property int|null $lop_id
 * @property string|null $ngay_sinh
 *
 * @property Khoa[] $khoas
 * @property Lop[] $lops
 * @property PhanAnhHienTruong[] $phanAnhHienTruongs
 * @property PhongBan[] $phongBans
 * @property ThongBao[] $thongBaos
 * @property ThongBao[] $thongBaos0
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 10;
    const STATUS_INACTIVE = 0;
    public const USERNOVERIFY = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vq_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'username', 'password_hash'], 'required'],
            [['id', 'status', 'lop_id'], 'integer'],
            [['created', 'updated', 'ngay_sinh', 'hoten'], 'safe'],
            [['ma_sinh_vien', 'username'], 'string', 'max' => 50],
            [['password_hash', 'auth_key', 'email', 'dia_chi'], 'string', 'max' => 100],
            [['ho_ten'], 'string', 'max' => 60],
            [['dien_thoai'], 'string', 'max' => 10],
            [['anh_dai_dien'], 'string', 'max' => 45],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ma_sinh_vien' => 'Mã sinh viên',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'ho_ten' => 'Họ tên',
            'dien_thoai' => 'Điện thoại',
            'email' => 'Email',
            'dia_chi' => 'Địa chỉ',
            'anh_dai_dien' => 'Anh Dai Dien',
            'status' => 'Status',
            'created' => 'Created',
            'updated' => 'Updated',
            'lop_id' => 'Lớp',
            'ngay_sinh' => 'Ngay Sinh',
        ];
    }

    /**
     * Gets query for [[Khoas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhoas()
    {
        return $this->hasMany(Khoa::className(), ['truong_khoa_id' => 'id']);
    }

    /**
     * Gets query for [[Lops]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLops()
    {
        return $this->hasMany(Lop::className(), ['lop_truong_id' => 'id']);
    }

    /**
     * Gets query for [[PhanAnhHienTruongs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPhanAnhHienTruongs()
    {
        return $this->hasMany(PhanAnhHienTruong::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[PhongBans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPhongBans()
    {
        return $this->hasMany(PhongBan::className(), ['truong_phong_id' => 'id']);
    }

    /**
     * Gets query for [[ThongBaos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getThongBaos()
    {
        return $this->hasMany(ThongBao::className(), ['user_created_id' => 'id']);
    }

    /**
     * Gets query for [[ThongBaos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getThongBaos0()
    {
        return $this->hasMany(ThongBao::className(), ['user_id' => 'id']);
    }


    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function beforeSave($insert)
    {

        if ($insert){
            $this->created = date("Y-m-d H:i:s");
        }
        else {
            $olduser = self::findOne($this->id);
            $oldPass = $olduser->password_hash;

            if($oldPass != $this->password_hash){
                $this->setPassword($this->password_hash);

                $session = Yii::$app->session;
                unset($session['old_id']);
                unset($session['timestamp']);
                $session->destroy();
            }
            $this->updated = date("Y-m-d H:i:s");
//            if ()
//            $ngaySinh = myAPI::convertDateSaveIntoDb($this->ngay_sinh);
//            $this->ngay_sinh = $ngaySinh;
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->id != 1) {
            $vaitro = Vaitrouser::findAll(['user_id' => $this->id]);
            foreach ($vaitro as $item) {
                $item->delete();
            }

            if(isset($_POST['Vaitrouser'])) {
                foreach ($_POST['Vaitrouser'] as $item) {
                    $vaitronguoidung = new Vaitrouser();
                    $vaitronguoidung->vai_tro_id = $item;
                    $vaitronguoidung->user_id = $this->id;
                    if(!$vaitronguoidung->save()){
                        throw new HttpException(500, Html::errorSummary($vaitronguoidung));
                    }
                }
            }
        }


        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public function isAccess($arrRoles){
        return !is_null(Vaitrouser::find()->andFilterWhere(['in', 'vaitro', $arrRoles])->andFilterWhere(['user_id' => Yii::$app->user->getId()])->one());
//        return 1;
    }


    public function beforeDelete()
    {
        Vaitrouser::deleteAll(['user_id' => $this->id]);
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}
