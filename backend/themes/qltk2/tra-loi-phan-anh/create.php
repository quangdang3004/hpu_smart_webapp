<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TraLoiPhanAnh */

?>
<div class="tra-loi-phan-anh-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
