<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "vq_phong_ban".
 *
 * @property int $id
 * @property string $ten_phong_ban
 * @property int $active
 * @property int $truong_phong_id
 *
 * @property User $truongPhong
 */
class PhongBan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vq_phong_ban';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ten_phong_ban', 'truong_phong_id'], 'required'],
            [['active', 'truong_phong_id'], 'integer'],
            [['ten_phong_ban'], 'string', 'max' => 100],
            [['truong_phong_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['truong_phong_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ten_phong_ban' => 'Ten Phong Ban',
            'active' => 'Active',
            'truong_phong_id' => 'Truong Phong ID',
        ];
    }

    /**
     * Gets query for [[TruongPhong]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTruongPhong()
    {
        return $this->hasOne(User::className(), ['id' => 'truong_phong_id']);
    }
}
