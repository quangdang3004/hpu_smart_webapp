<?php

namespace api\controllers;

use backend\controllers\CoreController;
use common\models\myAPI;
use Yii;
use backend\models\DanhMuc;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\helpers\Html;

class DanhMucController extends CoreApiController
{
    public function __construct($id, $module, $config = [])
    {
        $this->allowActions[] = 'get-list-data';
        parent::__construct($id, $module, $config);
    }

    // get-all-khu-vuc
    public function actionGetAllKhuVuc(){
        $query = DanhMuc::findAll(['active' => 1, 'type' => DanhMuc::KHU_VUC]);
        $data = [];
        foreach ($query as $item) {
            $data[] = ['label' => $item->ten_danh_muc, 'value' => $item->id];
        }
        return [
            'khuVuc' => $data
        ];
    }

    // get-data
    public function actionGetData(){
        $query = DanhMuc::find()->andFilterWhere(['active' => 1]);
        if (isset($this->dataPost['fieldsSearch'])){
            if (count($this->dataPost['fieldsSearch']['value']) > 0){
                $arrFieldSearch = ['ten_danh_muc', 'type'];
                foreach ($arrFieldSearch as $item){
                    if (trim($item) != '') {
                        $query->andFilterWhere(['like', $item, $this->dataPost['fieldsSearch']['value'][$item]]);
                    }
                }
            }
        }
        $totalCount = $query->count();
        $data = $query
            ->orderBy(['id'=> SORT_DESC])
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->limit($this->dataPost['limit'])
            ->andFilterWhere(['active' => 1])
            ->all();

        return [
            'results' => $data,
            'rows' => $totalCount
        ];
    }

    /** load */
    public function actionLoad(){
        $user = DanhMuc::find()->andFilterWhere(['id' => $this->dataPost['danh_muc'], 'active' => 1])
            ->one();
        $user->type = ['value' => $user->type, 'label' => $user->type];
        if(!is_null($user))
            return  $user;
        throw new HttpException(500, 'Không tìm thấy dữ liệu tương ứng');
    }

    /** save */
    public function actionSave(){
        if($this->dataPost['id'] == '')
            $model = new DanhMuc();
        else
            $model = DanhMuc::findOne($this->dataPost['id']);

        $model->name = $this->dataPost['name'];
        if (isset($this->dataPost['code'])){
            if ($this->dataPost['code'] != ''){
                $model->code = $this->dataPost['code'];
            }
        }
        $model->type = $this->dataPost['type']['value'];

        if($model->save())
            return [
                'content' => 'Cập nhật thông tin danh mục thành công'
            ];
        throw new HttpException(500, Html::errorSummary($model));
    }

    /** delete */
    public function actionDelete(){
        $dataPost = myAPI::getDataPost();
        DanhMuc::updateAll(['active' => 0], ['id' => $dataPost['danh_muc']]);
        return [
            'message' => 'Xóa dữ liệu thành công',
        ];
    }

    // get-list-data
    public function actionGetListData(){
        $data = DanhMuc::findAll(['type' => $this->dataPost['type']]);
        $results = [];
        foreach ($data as $item) {
            $results[] = ['key' => $item->id, 'label' => $item->ten_danh_muc];
        }

        return [
            'results' => $results,
        ];
    }
}
