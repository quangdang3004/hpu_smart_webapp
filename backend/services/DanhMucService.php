<?php

namespace backend\services;

use backend\models\DanhMuc;

class DanhMucService
{
    /**
     * Lấy danh mục bằng type
     * @param $type
     * @return DanhMuc[]
     */
    public static function getDanhMucByType($type, $field = '*'){
        $danhMuc = DanhMuc::find()->select($field)->andFilterWhere(['type' => $type, 'active' => 1])->all();
        return $danhMuc;
    }

    /**
     * Lấy danh mục trả về format của select2 flutter
     * @param $type
     * @return array[]
     */
    public static function getDanhMucSelect2($type){
//        $responseBody = DanhMuc::find()->select(['id AS category_id', 'ten_danh_muc AS category_name'])->andFilterWhere(['type' => $type, 'active' => 1])->all();
        $responseBody = \Yii::$app->db->createCommand("Select `id` AS `category_id`, `ten_danh_muc` AS `category_name` from vq_danh_muc where type = '$type' and active = 1")->queryAll();
        return [
            'responseBody' => $responseBody,
            'responseTotalResult' => count($responseBody)
        ];
    }

}