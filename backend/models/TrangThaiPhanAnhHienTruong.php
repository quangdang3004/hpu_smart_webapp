<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vq_trang_thai_phan_anh_hien_truong".
 *
 * @property int $id
 * @property string|null $trang_thai
 * @property string $created
 * @property string|null $dete
 * @property int $vq_phan_anh_hien_truong_id
 *
 * @property PhanAnhHienTruong $vqPhanAnhHienTruong
 */
class TrangThaiPhanAnhHienTruong extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vq_trang_thai_phan_anh_hien_truong';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['trang_thai'], 'string'],
            [['created'], 'safe'],
            [['vq_phan_anh_hien_truong_id'], 'required'],
            [['vq_phan_anh_hien_truong_id'], 'integer'],
            [['dete'], 'string', 'max' => 45],
            [['vq_phan_anh_hien_truong_id'], 'exist', 'skipOnError' => true, 'targetClass' => PhanAnhHienTruong::className(), 'targetAttribute' => ['vq_phan_anh_hien_truong_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trang_thai' => 'Trang Thai',
            'created' => 'Created',
            'dete' => 'Dete',
            'vq_phan_anh_hien_truong_id' => 'Vq Phan Anh Hien Truong ID',
        ];
    }

    /**
     * Gets query for [[VqPhanAnhHienTruong]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVqPhanAnhHienTruong()
    {
        return $this->hasOne(PhanAnhHienTruong::className(), ['id' => 'vq_phan_anh_hien_truong_id']);
    }

}
