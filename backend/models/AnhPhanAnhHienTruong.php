<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%anh_phan_anh_hien_truong}}".
 *
 * @property int $id
 * @property string $image
 * @property int|null $phan_anh_id
 * @property int|null $tra_loi_phan_anh_id
 * @property bool $active
 *
 * @property PhanAnhHienTruong $phanAnh
 * @property TraLoiPhanAnh $traLoiPhanAnh
 */
class AnhPhanAnhHienTruong extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%anh_phan_anh_hien_truong}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['phan_anh_id', 'tra_loi_phan_anh_id'], 'integer'],
            [['active'], 'boolean'],
            [['image'], 'string', 'max' => 100],
            [['phan_anh_id'], 'exist', 'skipOnError' => true, 'targetClass' => PhanAnhHienTruong::className(), 'targetAttribute' => ['phan_anh_id' => 'id']],
            [['tra_loi_phan_anh_id'], 'exist', 'skipOnError' => true, 'targetClass' => TraLoiPhanAnh::className(), 'targetAttribute' => ['tra_loi_phan_anh_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'phan_anh_id' => 'Phan Anh ID',
            'tra_loi_phan_anh_id' => 'Tra Loi Phan Anh ID',
            'active' => 'Active',
        ];
    }

    /**
     * Gets query for [[PhanAnh]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPhanAnh()
    {
        return $this->hasOne(PhanAnhHienTruong::className(), ['id' => 'phan_anh_id']);
    }

    /**
     * Gets query for [[TraLoiPhanAnh]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTraLoiPhanAnh()
    {
        return $this->hasOne(TraLoiPhanAnh::className(), ['id' => 'tra_loi_phan_anh_id']);
    }
}
