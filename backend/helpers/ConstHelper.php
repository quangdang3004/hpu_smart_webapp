<?php
namespace backend\helpers;

class ConstHelper{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const BUTTON_LABEL_CLOSE = 'Đóng';
    const BUTTON_LABEL_BACK = 'Quay lại';
    const BUTTON_LABEL_SAVE = 'Lưu';
    const BUTTON_LABEL_CREATE = 'Thêm';
    const BUTTON_LABEL_EDIT = 'Sửa';
    const BASE_URL = "https://hpusmart.info";

}