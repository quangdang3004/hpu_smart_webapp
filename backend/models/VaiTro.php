<?php

namespace backend\models;

use common\models\myActiveRecord;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%vai_tro}}".
 *
 * @property integer $id
 * @property string $ten_vai_tro
 *
 * @property PhanQuyen[] $phanQuyens
 * @property Vaitrouser[] $vaitrousers
 */
class VaiTro extends ActiveRecord
{
    const QUAN_LY = 'Quản lý';
    const NHAN_VIEN = 'Nhân viên';
    const GIAM_DOC = 'Giám đốc';
    const PHO_GIAM_DOC = 'Phó giám đốc';
    const TRUONG_PHONG = 'Trưởng phòng';
    const PHONG_HCNS = 'Phòng hành chính nhân sự';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vq_vai_tro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ten_vai_tro'], 'required'],
            [['ten_vai_tro'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhanQuyens()
    {
        return $this->hasMany(PhanQuyen::className(), ['vai_tro_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVaitrousers()
    {
        return $this->hasMany(Vaitrouser::className(), ['vai_tro_id' => 'id']);
    }
}
