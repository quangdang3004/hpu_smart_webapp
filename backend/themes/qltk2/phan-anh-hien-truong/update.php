<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PhanAnhHienTruong */
?>
<div class="phan-anh-hien-truong-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
