<?php
namespace backend\services;

use backend\models\CauHinh;
use yii\db\ActiveRecord;

class CauHinhService {

    /**
     * Lấy cấu hình theo ký hiệu
     * @param string $keyHieu
     * @return null|CauHinh|ActiveRecord
     */
    public static function getCauHinhByKyHieu($kyHieu) {
        return CauHinh::find()->where(['ghi_chu' => $kyHieu])->one();
    }

    /**
     * Lấy value theo ký hiệu
     * @param $kyHieu
     * @return mixed|string|null
     */
    public static function getValueByKyHieu($kyHieu) {
        $cauHinh = CauHinhService::getCauHinhByKyHieu($kyHieu);
        if($cauHinh){
            return $cauHinh->content;
        }
        return null;
    }
}