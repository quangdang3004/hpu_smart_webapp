<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TraLoiPhanAnh */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tra-loi-phan-anh-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'noi_dung')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <?= $form->field($model, 'file_dinh_kem')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hinh_anh')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phan_anh_hien_truong_id')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
