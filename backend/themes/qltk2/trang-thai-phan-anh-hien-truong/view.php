<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TrangThaiPhanAnhHienTruong */
?>
<div class="trang-thai-phan-anh-hien-truong-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'trang_thai',
            'created',
            'dete',
            'vq_phan_anh_hien_truong_id',
        ],
    ]) ?>

</div>
