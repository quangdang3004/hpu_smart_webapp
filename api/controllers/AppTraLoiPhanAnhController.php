<?php

namespace api\controllers;

use backend\helpers\ConstHelper;
use backend\models\AnhPhanAnhHienTruong;
use backend\models\PhanAnhHienTruong;
use backend\models\TraLoiPhanAnh;
use backend\services\TrangThaiPhanAnhService;
use yii\helpers\Html;
use yii\web\HttpException;

class AppTraLoiPhanAnhController extends CoreApiController
{
    public function __construct($id, $module, $config = [])
    {
//        $this->allowActions[] = '';
        parent::__construct($id, $module, $config);
    }

    public function actionGetData(){
        $query = TraLoiPhanAnh::find();
        if (isset($this->dataPost['fieldsSearch'])){
            if (count($this->dataPost['fieldsSearch']['value']) > 0){
                $arrFieldSearch = ['id', 'phan_anh_hien_truong_id'];
                foreach ($arrFieldSearch as $item){
                    if (trim($item) != '') {
                        $query->andFilterWhere(['like', $item, $this->dataPost['fieldsSearch']['value'][$item]]);
                    }
                }
            }
        }

        $totalCount = $query->count();
        $data = $query
            ->orderBy(['id'=> SORT_DESC])
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->limit($this->dataPost['limit'])
            ->andFilterWhere(['active' => 1])
            ->all();

        foreach ($data as $item){
            $hinhAnh = AnhPhanAnhHienTruong::findOne(['tra_loi_phan_anh_id' => $item->id]);
//            VarDumper::dump($item->id);exit();
            if (!is_null($hinhAnh))
                $item->hinh_anh = ConstHelper::BASE_URL.$hinhAnh->image;
        }
        return [
            'results' => $data,
            'rows' => $totalCount
        ];
    }


    /** load */
    public function actionLoad(){
        $model = TraLoiPhanAnh::findOne(['id' => $this->dataPost['tra_loi_phan_anh_id']]);
        $anhPhanAnh = AnhPhanAnhHienTruong::findAll(['tra_loi_phan_anh_id' => $this->dataPost['tra_loi_phan_anh_id']]);
        foreach ($anhPhanAnh as $item){
            $item->image = ConstHelper::BASE_URL.$item->image;
        }
        return [
            'result' => $model,
            'anhPhanAnh' => $anhPhanAnh
        ];
    }

    /** delete */
    public function actionDelete(){
        $model = TraLoiPhanAnh::findOne($this->dataPost['tra_loi_phan_anh_id']);
        TrangThaiPhanAnhService::createTrangThaiPhanAnhWithTrangThaiAndPhanAnhID(PhanAnhHienTruong::CHO_DUYET, $model->phan_anh_hien_truong_id);
        $anhTraLoi = AnhPhanAnhHienTruong::findAll(['tra_loi_phan_anh_id' => $model->id]);
        foreach ( $anhTraLoi as $item){
            $item->updateAttributes(['active' => 0]);
        }
        $model->active = 0;
        if($model->save())
            return [
                'message' => 'Đã xóa trả lời '.$model->id.' thành công',
            ];
        else
            throw new HttpException(500, Html::errorSummary($model));
    }

}