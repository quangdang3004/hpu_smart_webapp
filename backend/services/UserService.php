<?php

namespace backend\services;

use common\models\User;
use yii\db\ActiveRecord;

class UserService {
    /**
     * Lấy thông tin người dùng theo id
     * @param $userId int
     * @param $fields string[]
     * @return User|ActiveRecord|null
     */
    public static function getUserById($userId, $fields = "*") {
        return User::find()->select($fields)->where(['id' => (int)$userId, 'status' => User::STATUS_ACTIVE])->one();
    }

    /**
     * Xoá người dùng bởi ID
     * @param int $id
     * @return boolean
     */
    public static function deleteUserById($id) {
        $user = UserService::getUserById($id);
        if(isset($user)){
            $user->status = User::STATUS_INACTIVE;
            return $user->save();
        }
        return false;
    }

    /**
     * Get general password hash
     * @param string $password
     * @return string
     * @throws \yii\base\Exception
     */
    public static function getGeneralPasswordHash($password){
        return \Yii::$app->security->generatePasswordHash($password);
    }

}