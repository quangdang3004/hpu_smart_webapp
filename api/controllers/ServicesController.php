<?php

namespace api\controllers;

use api\models\TempSecret;
use backend\helpers\ConstHelper;
use backend\models\AnhPhanAnhHienTruong;
use backend\models\Lop;
use backend\models\OtpUser;
use backend\models\PhanAnhHienTruong;
use backend\models\QuanLyPhanAnhHienTruong;
use backend\models\UserVaiTro;
use backend\models\Vaitrouser;
use backend\services\CauHinhService;
use common\models\myAPI;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\Cookie;
use yii\web\HttpException;

class ServicesController extends AndinCoreApiController
{
    /** login */
    public function actionLoginWeb(){
        if(!isset($_POST['username']))
            throw new HttpException(500, 'Không có thông tin tên đăng nhập');
        else if(!isset($_POST['password']))
            throw new HttpException(500, 'Không có thông tin mật khẩu');
        else{
            if(empty($_POST['username']) || empty($_POST['password'])){
                throw new HttpException(500, 'Tên đăng nhập hoặc số điện thoại và mật khẩu không được để trống');
            }else{
                /** @var User $user */
                $user = User::find()
                    ->andWhere('username = :u or dien_thoai = :u', [
                        ':u' => $_POST['username']
                    ])
                    ->andFilterWhere(['status' => 10])
                    ->one();
                if(is_null($user)){
                    throw new HttpException(500, 'Tên đăng nhập hoặc số điện thoại không đúng');
                }else{
                    if(\Yii::$app->security->validatePassword($_POST['password'], $user->password_hash)){
                        $auth = \Yii::$app->security->generateRandomString(32);
                        $user->updateAttributes(['auth_key' => $auth]);

                        $cookies = \Yii::$app->response->cookies;
                        $cookies->add(new Cookie([
                            'name' => 'token',
                            'value' => $auth
                        ]));
                        $cookies->add(new Cookie([
                            'name' => 'username',
                            'value' => \Yii::$app->security->generateRandomString()
                        ]));
                        $cookies->add(new Cookie([
                            'name' => 'userId',
                            'value' => $user->username
                        ]));

                        return [
                            'message' => 'Đăng nhập thành công',
                            'uid' => $user->id,
                            'auth' => $auth,
                            'user' => User::findOne($user->id),
                        ];
                    }
                    else
                        throw new HttpException(500, 'Mật khẩu không đúng');
                }
            }
        }
    }
    public function actionGetTempSecret()
    {
        $tempSecret = str_replace(['_', '-'], 'a', \Yii::$app->security->generateRandomString(32));
        $secret = new TempSecret();
        $secret->key = $tempSecret;
        $secret->expire = time() + 600;// Tạo private key hạn 10p
        if (!$secret->save()) {
            throw new HttpException(500, Html::errorSummary($secret));
        }
        return [
            'data' => $tempSecret
        ];
    }

    public function actionLogin()
    {
        if ($_POST['ma_sinh_vien'] == null || $_POST['password'] == null) {
            throw new HttpException(500, 'Mã sinh viên hoặc mật khẩu không hợp lệ');
        }
        $user = User::findOne(['ma_sinh_vien' => $_POST['ma_sinh_vien']]);
        if ($user == null) {
            throw new HttpException(500, 'Mã sinh viên hoặc mật khẩu không hợp lệ');
        }
        if (!\Yii::$app->security->validatePassword($_POST['password'], $user->password_hash))
            throw new HttpException(500, 'Mã sinh viên hoặc mật khẩu không hợp lệ');
        $user->updateAttributes(['auth_key' => 'DVQ_' . \Yii::$app->security->generateRandomString(18)]);
        $user->updateAttributes(['secret_key' => str_replace(['_', '-'], 'a', \Yii::$app->security->generateRandomString(32))]);
        $user = UserVaiTro::find()->select(['id', 'username', 'ho_ten', 'ngay_sinh', 'email', 'dien_thoai', 'ma_sinh_vien', 'auth_key', 'lop_id', 'ten_lop', 'ten_khoa', 'ten_vai_tro', 'status'])
            ->andFilterWhere(['id' => $user->id])->one();
        $user->ngay_sinh = myAPI::covertYMD2DMY($user->ngay_sinh);

        if ($user->status == 0){
            return [
                'status' => 0,
                'user' => $user,
                'message' => 'Tài khoản chưa được kích hoạt'
            ];
        }
        return [
            'message' => 'Đăng nhập thành công',
            'user' => $user,
            'status' => 10
        ];
    }


    public function actionQuenMatKhau()
    {
        if (!isset($_POST['email']))
            $this->error500('Không tìm thấy tài khoản của bạn');
        if ($_POST['email'] == "")
            $this->error500('Vui lòng điền email');
        $user = User::findOne(['email' => $_POST['email']]);
        if (is_null($user))
            $this->error500('Không tìm thấy tài khoản của bạn');
        if (is_null(OTPUser::find()->andFilterWhere(['user_id' => $user->id, 'is_used' => 1])->andFilterWhere(['>=', 'exp_otp', time()])->one())) {
            if (!filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                $this->error500('Email của bạn không có hoặc không đúng định dạng, vui lòng liên hệ quản trị viên hệ thống để được hỗ trợ');
            }
            $otp = new OTPUser();
            $otp->user_id = $user->id;
            if (!$otp->save())
                $this->error500(Html::errorSummary($otp));
            $email = explode('@', $user->email);
            $replace_email = substr($email[0], 0, 3) . '****@' . $email[1];
            return [
                'message' => 'Đã gửi mã xác thực tới email ' . $replace_email . '!',
                'user' => $user
            ];
        }
        return null;
    }

    public function actionCheckOtp()
    {
        if (!isset($_POST['email']))
            $this->error500('Email không đúng vui lòng thử lại!');
        if ($_POST['email'] == "")
            $this->error500('Email không đúng vui lòng thử lại!');
        if (!isset($_POST['key_otp']))
            $this->error500('OTP không đúng vui lòng thử lại!');
        if ($_POST['key_otp'] == "")
            $this->error500('OTP không đúng vui lòng thử lại!');
        $user_forget = User::findOne(['email' => $_POST['email']]);
        if (is_null($user_forget))
            $this->error500('Email không đúng vui lòng thử lại');
        $check_OTP = OTPUser::find()
            ->andFilterWhere(['user_id' => $user_forget->id, 'otp' => $_POST['key_otp'], 'is_used' => 0])->one();
        if (is_null($check_OTP))
            $this->error500('OTP không đúng vui lòng thử lại!');
        if (time() - $check_OTP->exp_otp > 120) {
            $this->error500('Mã OTP đã hết hạn');
        }
        $secet_key = \Yii::$app->security->generateRandomString(32);
        $check_OTP->updateAttributes([
            'is_used' => 1,
            'secret_key' => $secet_key,
            'exp_key' => time() + 60,
        ]);
        $user_forget->updateAttributes(['status' => 10]);
        return [
            'key' => $secet_key
        ];

    }

    public function actionChangePassword()
    {
        $user = User::findOne(['id' => $_POST['uid'], 'auth_key' => $_POST['auth'], 'status' => User::STATUS_ACTIVE]);
        if (!isset($user))
            $this->error500('Người dùng không tồn tại!');
        $user->updateAttributes(['password_hash' => \Yii::$app->security->generatePasswordHash($_POST['new_pass'])]);
        return [
            'message' => 'Đổi mật khẩu thành công, vui lòng đăng nhập lại để tiêp tục sử dụng phần mềm',
            'user' => $user
        ];

    }

    public function actionDoiPassOtp()
    {
        if (!isset($_POST['email']))
            $this->error500('Email không đúng vui lòng thử lại!');
        if ($_POST['email'] == "")
            $this->error500('Email không đúng vui lòng thử lại!');
        if (!isset($_POST['new_pass']))
            $this->error500('Vui lòng điền mật khẩu mới');
        if (!$this->checkLeght($_POST['new_pass'], 6, 32))
            $this->error500('Mật khẩu phải có độ dài lớn hơn 6 và nhỏ hơn 32 kí tự');
        if (!isset($_POST['key_otp']))
            $this->error500('Thời gian đổi mật khẩu của bạn đã hết!');
        if ($_POST['key_otp'] == "")
            $this->error500('Thời gian đổi mật khẩu của bạn đã hết!');
        $user_forget = User::findOne(['email' => $_POST['email'], 'status' => 10]);
        if (is_null($user_forget))
            $this->error500('Email không đúng vui lòng thử lại');
        $check_OTP = OTPUser::find()
            ->andFilterWhere(['user_id' => $user_forget->id, 'secret_key' => $_POST['key_otp'], 'is_used' => 1])
            ->one();
        if (is_null($check_OTP))
            $this->error500('Có lỗi sảy ra vui lòng thử lại!');
        if (time() - $check_OTP->exp_key > 60)
            $this->error500('Thời gian đổi mật khẩu của bạn đã hết!');
        $user_forget->updateAttributes(['password_hash' => \Yii::$app->security->generatePasswordHash($_POST['new_pass'])]);
        $check_OTP->updateAttributes(['exp_key' => 0]);
        return ['message' => 'Đổi mật khẩu thành công!'];
    }

    public function actionLoadUser()
    {
        $user = UserVaiTro::findOne(['id' => $_POST['uid']]);
        return [
            'user' => $user,
        ];
    }

    public function actionDangKi()
    {
        $User = new  User();
        $arrFildes = ['ma_sinh_vien', 'email', 'ho_ten', 'dien_thoai'];
        foreach ($arrFildes as $item) {
            if (!isset($_POST[$item])) {
                throw new HttpException(500, 'Vui lòng nhập đầy đủ ' . $User->getAttributeLabel($item));
            }
        }
        $userCheck = User::findOne(['ma_sinh_vien' => $_POST['ma_sinh_vien']]);
        if (isset($userCheck))
            $this->error500('Mã sinh viên đã được đăng kí');

        $userCheck = User::findOne(['email' => $_POST['email']]);
        if (isset($userCheck))
            $this->error500('Email đã được đăng kí');

        $userCheck = User::findOne(['dien_thoai' => $_POST['dien_thoai']]);
        if (isset($userCheck))
            $this->error500('Số điện thoại đã được đăng kí');

        if (strlen($_POST['dien_thoai']) != 10)
            $this->error500('Số điện thoại phải có 10 số');

        foreach ($arrFildes as $item) {
            if (($_POST[$item]) == '') {
                throw new HttpException(500, 'Vui lòng nhập đầy đủ ' . $User->getAttributeLabel($item));
            } else {
                $User->$item = $_POST[$item];
            }

        }
        $User->username = $_POST['ma_sinh_vien'];
        $User->lop_id = $_POST['lop'];
        $User->password_hash = \Yii::$app->security->generatePasswordHash($_POST['password']);
        $User->status = 0;

        if ($User->save()) {
            //OTP
            $otp = new OTPUser();
            $otp->user_id = $User->id;
            if (!$otp->save())
                $this->error500(Html::errorSummary($otp));
            $email = explode('@', $User->email);
            $replace_email = substr($email[0], 0, 3) . '****@' . $email[1];

            //Vai trò
            $vaitro = new Vaitrouser();
            $vaitro->user_id = $User->id;
            $vaitro->vai_tro_id = User::USERNOVERIFY;
            if (!$vaitro->save()) {
                throw new HttpException(500, Html::errorSummary($vaitro));
            }

            return [
                'message' => 'Đã gửi mã xác thực tới email ' . $replace_email . '!',
                'email' => $User->email
            ];
        } else {
            throw new HttpException(500, Html::errorSummary($User));

        }
    }

    public function actionDangXuat()
    {
        $user = User::findOne($_POST['uid']);
        if (!is_null($user)) {
            $user->updateAttributes(['auth_key' => null]);
        }
        return [
            'message' => 'Đăng xuất thành công'
        ];
    }

    public function actionHuongDanVaLienHe() {
        $huongDan = CauHinhService::getValueByKyHieu('huong_dan');
        $lienHe = CauHinhService::getValueByKyHieu('lien_he');

        return [
            'huongDan' => $huongDan,
            'lienHe' => $lienHe
        ];
    }

    public function actionDanhSachPhanAnhHienTruong(){
        $phanAnh = QuanLyPhanAnhHienTruong::find()->orderBy(['created' => SORT_DESC])->all();
        foreach ($phanAnh as $item){
            $item->created = date('Y/m/d H:i', strtotime($item->created));
            $hinhAnh = AnhPhanAnhHienTruong::findOne(['phan_anh_id' => $item->id]);
//            VarDumper::dump($item->id);exit();
            if (!is_null($hinhAnh))
                $item->hinh_anh = ConstHelper::BASE_URL.$hinhAnh->image;
        }
        return [
            'phanAnhHienTruong' => $phanAnh
        ];
    }

    public function actionGetLop() {

        $responseBody = \Yii::$app->db->createCommand("Select `id` AS `category_id`, `name` AS `category_name` from vq_lop")->queryAll();
        return [
            'responseBody' => $responseBody,
            'responseTotalResult' => count($responseBody)
        ];
    }
}