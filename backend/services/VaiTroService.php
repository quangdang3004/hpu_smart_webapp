<?php

namespace backend\services;

use backend\models\VaiTro;
use function React\Promise\all;

class VaiTroService
{
    public static function GetAllVaiTro(){
        return VaiTro::find()->all();
    }
}