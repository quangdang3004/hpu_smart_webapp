<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'formatter' => [
            'dateFormat' => 'MM/dd/yyyy',
            'decimalSeparator' => '.',
            'thousandSeparator' => ' ',
            'currencyCode' => 'VND',
        ],
        'request' => [
            'enableCookieValidation' => true,

            'enableCsrfValidation' => true,

            'cookieValidationKey' => 'xxxxxxx',
        ]
    ],

];
