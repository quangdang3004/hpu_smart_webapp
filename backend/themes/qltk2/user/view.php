<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */
?>
<div class="user-view">
    <p>
        <strong>Họ tên: </strong><?=$model->ho_ten?>
    </p>
    <p>
        <strong>Điện thoại: </strong><?=$model->dien_thoai?>
    </p>
</div>
