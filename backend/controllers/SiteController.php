<?php
namespace backend\controllers;

use backend\models\AnhTienChi;
use backend\models\BangChamCong;
use backend\models\CanDoiTaiChinh;
use backend\models\CauHinh;
use backend\models\ChiaSeKhachHang;
use backend\models\ChiTietTienChi;
use backend\models\DanhMuc;
use backend\models\GhiChu;
use backend\models\LichSuChi;
use backend\models\LichSuDoanhThu;
use backend\models\QuanLyKhachHang;
use backend\models\QuanLySanPham;
use backend\models\QuanLyTuyenDungNhuCauNsUser;
use backend\models\SanPham;
use backend\models\TaiChinh;
use backend\models\ThongTinBanHang;
use backend\models\ThuongPhat;
use backend\models\TienChi;
use backend\models\TrangThaiSanPham;
use backend\models\TuyenDungDkNhuCauNs;
use common\models\myAPI;
use common\models\User;
use Yii;
use yii\base\Security;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\HttpException;
use yii\web\Response;

class SiteController extends CoreController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'update-hotline'],
                        'allow' => true,
//                        'roles' => ['?']
                    ],
                    [
                        'actions' => ['error'],
                        'allow' => true
                    ],
                    [
                        'actions' => ['logout', 'loadform', 'doimatkhau', 'index','danh-sach-khach-hang', 'update-san-pham',],
                        'allow' => true,
//                        'matchCallback' => function($rule, $action){
//                            return Yii::$app->user->identity->username == 'adamin';
//                        }
                        'roles' => ['@']
                    ],
                ],
//                'denyCallback' => function ($rule, $action) {
//                    throw new Exception('You are not allowed to access this page', 404);
//                }
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /** index */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /** login */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->renderPartial('login', [
                'model' => $model,
            ]);
        }
    }

    /** logout */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /** error */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return myAPI::getMessage($exception->getMessage(),'danger', "Thông báo!");
            }
        }
        return $this->render('error', ['exception' => $exception]);
    }

    /** doimatkhau */
    public function actionDoimatkhau(){
        $sercurity = new Security();
        $user = User::findOne(Yii::$app->user->getId());
        if (!Yii::$app->security->validatePassword($_POST['matkhaucu'], $user->password_hash))
            throw new HttpException(500, 'Mật khẩu cũ không đúng!');
        else{
            $matkhaumoi = $sercurity->generatePasswordHash(trim($_POST['matkhaumoi']));
            User::updateAll(['password_hash' => $matkhaumoi], ['id' => Yii::$app->user->getId()]);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'message' => 'Đổi mật khẩu thành công'
            ];
        }
    }

    //load-form-modal

//    public function actionLoadform()
//    {
//        $content = '';
//        $header = '';
//        if (isset($_POST['type'])) {
//
//            if ($_POST['type'] == 'duyet_phieu_tuyen_dung') {
//                $title = "Bạn muốn duyệt phiếu đăng kí tuyển dụng nhân sự này?";
//                $model = QuanLyTuyenDungNhuCauNsUser::findOne(['id' => $_POST['id']]);
//                Yii::$app->response->format = Response::FORMAT_JSON;
//                return [
//                    'content' => $this->renderAjax('../tuyen-dung-dk-nhu-cau-ns/form_pop_up', [
//                        'model' => $model,
//                    ]),
//                    'title' => $title
//                ];
//            }
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            return $this->renderAjax('_modal', [
//                'content' => $content,
//                'header' => $header
//            ]);
//        } else
//            throw new HttpException(500, 'Không có thông tin loại modal cần load');
//    }



}
