<?php

namespace api\controllers;

use common\models\myAPI;
use yii\helpers\Json;

class SiteController extends CoreApiController
{
    public function __construct($id, $module, $config = [])
    {
        $this->allowActions[] = 'get-menu';
        parent::__construct($id, $module, $config);
    }

    //get-menu
    public function actionGetMenu(){
        $menu = [
            [
                'title' => 'Tổng quan ',
                'route' => 'dashboard-ecommerce',
                'icon' => 'LayersIcon',
            ],
        ];

        if(
            myAPI::isAccess2('DanhMuc','Get-data')
        ){
            $menu[] = ['header' => 'Danh mục'];
            if(myAPI::isAccess2('DanhMuc','Get-data'))
                $menu[] = ["route" => "danh-muc", "title" => "Danh mục chung", "icon" => "MenuIcon"];
        }

        if (myAPI::isAccess2('AppPhanAnhHienTruong', 'Get-data')){
            $menu[] = ['header' => 'Phản ánh hiện trường'];
            $menu[] = ["route" => "phan-anh-hien-truong", "title" => "Phản ánh hiện trường", "icon" => "SendIcon"];
        }
        if (myAPI::isAccess2('AppTraLoiPhanAnh', 'Get-data')){
            $menu[] = ["route" => "tra-loi-phan-anh", "title" => "Trả lời phản ánh", "icon" => "MessageSquareIcon"];
        }
        if (myAPI::isAccess2('AppLop', 'Get-data')){
            $menu[] = ["route" => "lop", "title" => "Quản lý lớp", "icon" => "BuildingIcon"];
        }
        if (myAPI::isAccess2('AppKhoa', 'Get-data')){
            $menu[] = ["route" => "khoa", "title" => "Quản lý khoa", "icon" => "BuildingIcon"];
        }
        if (myAPI::isAccess2('AppPhongBan', 'Get-data')){
            $menu[] = ["route" => "phong-ban", "title" => "Quản lý phòng ban", "icon" => "BuildingIcon"];
        }
        if (myAPI::isAccess2('AppBinhLuanPhanAnh', 'Get-data')){
            $menu[] = ["route" => "binh-luan-phan-anh", "title" => "Quản lý bình luận", "icon" => "BuildingIcon"];
        }

        if(   myAPI::isAccess2('User','Get-data')
            || myAPI::isAccess2('VaiTro','Get-data')
            || myAPI::isAccess2('Cauhinh','Get-data')
            || myAPI::isAccess2('ChucNang','Get-data')
            || myAPI::isAccess2('PhanQuyen','Index')
            || myAPI::isAccess2('User','Profile')
            ){
                $menu[] = ['header' => 'Hệ thống'];

            if(myAPI::isAccess2('User','Index-thanh-vien') && myAPI::isAccess2('User','Get-data') )
                $menu[] = ["route" => "thanh-vien", "title" => "Thành viên", "icon" => "UsersIcon"];
            if(myAPI::isAccess2('VaiTro','Get-data'))
                $menu[] = ["route" => "vai-tro", "title" => "Vai trò", 'icon' => 'AwardIcon'];
            if(myAPI::isAccess2('ChucNang','Get-data'))
                $menu[] = ["route" => "chuc-nang", "title" => "Chức năng", "icon" => "ToolIcon"];
            if(myAPI::isAccess2('PhanQuyen','Index'))
                $menu[] = ["route" => "phan-quyen", "title" => "Phân Quyền", "icon" => "KeyIcon"];
            if(myAPI::isAccess2('Cauhinh','Get-data'))
                $menu[] = ["route" => "cau-hinh", "title" => "Cấu hình", 'icon' => 'SettingsIcon'];
            if(myAPI::isAccess2('User','Profile'))
                $menu[] = ["route" => "profile", "title" => "Hồ sơ cá nhân", 'icon' => 'UsersIcon'];
        }
        return [
            'menu' => $menu,
        ];
    }
}