<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%quan_ly_phan_anh_hien_truong}}".
 *
 * @property int $id
 * @property string $noi_dung Nội dung phản ánh hiện trường
 * @property bool|null $khan Yêu cầu phản ánh có khẩn hay không
 * @property string|null $hinh_anh Hình ảnh phản ánh
 * @property string|null $file_dinh_kem File đính kèm
 * @property string $trang_thai
 * @property int $muc_do_phan_anh_id
 * @property int $phong_ban_id
 * @property int $khu_vuc_id
 * @property int $chuyen_muc_id
 * @property string $created
 * @property string|null $updated
 * @property int $user_id
 * @property int $active
 * @property string|null $ten_khu_vuc
 * @property string|null $muc_do
 * @property string|null $ten_phong_ban
 * @property string|null $chuyen_muc
 */
class QuanLyPhanAnhHienTruong extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%quan_ly_phan_anh_hien_truong}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'muc_do_phan_anh_id', 'phong_ban_id', 'khu_vuc_id', 'user_id', 'active', 'chuyen_muc_id'], 'integer'],
            [['noi_dung', 'muc_do_phan_anh_id', 'phong_ban_id', 'khu_vuc_id', 'created', 'user_id'], 'required'],
            [['noi_dung', 'trang_thai'], 'string'],
            [['khan'], 'boolean'],
            [['created', 'updated'], 'safe'],
            [['hinh_anh', 'file_dinh_kem'], 'string', 'max' => 300],
            [['ten_khu_vuc', 'muc_do'], 'string', 'max' => 50],
            [['ten_phong_ban', 'chuyen_muc'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'noi_dung' => 'Noi Dung',
            'khan' => 'Khan',
            'hinh_anh' => 'Hinh Anh',
            'file_dinh_kem' => 'File Dinh Kem',
            'trang_thai' => 'Trang Thai',
            'muc_do_phan_anh_id' => 'Muc Do Phan Anh ID',
            'phong_ban_id' => 'Phong Ban ID',
            'khu_vuc_id' => 'Khu Vuc ID',
            'created' => 'Created',
            'updated' => 'Updated',
            'user_id' => 'User ID',
            'active' => 'Active',
            'ten_khu_vuc' => 'Ten Khu Vuc',
            'muc_do' => 'Muc Do',
            'ten_phong_ban' => 'Ten Phong Ban',
        ];
    }
}
