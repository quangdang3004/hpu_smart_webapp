<?php

namespace backend\models;

use api\models\User;
use Yii;

/**
 * This is the model class for table "vq_thong_bao".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $noi_dung
 * @property string $created
 * @property string $time_seen Thời gian xem thông báo
 * @property bool $is_seen Trạng thái thông báo đã xem hay chưa
 * @property int $user_id
 * @property int $user_created_id
 *
 * @property User $userCreated
 * @property User $user
 */
class ThongBao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vq_thong_bao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['noi_dung'], 'string'],
            [['created', 'time_seen', 'user_id', 'user_created_id'], 'required'],
            [['created', 'time_seen'], 'safe'],
            [['is_seen'], 'boolean'],
            [['user_id', 'user_created_id'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['user_created_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_created_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'noi_dung' => 'Noi Dung',
            'created' => 'Created',
            'time_seen' => 'Time Seen',
            'is_seen' => 'Is Seen',
            'user_id' => 'User ID',
            'user_created_id' => 'User Created ID',
        ];
    }

    /**
     * Gets query for [[UserCreated]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_created_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
