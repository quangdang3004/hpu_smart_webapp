<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PhanAnhHienTruong */

?>
<div class="phan-anh-hien-truong-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
