<?php return array(
    'root' => array(
        'name' => 'kartik-v/yii2-widget-datetimepicker',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '0882ff90f7c59bb89894b09e31ec7ad2fd8a0107',
        'type' => 'yii2-extension',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'kartik-v/yii2-krajee-base' => array(
            'pretty_version' => 'v3.0.5',
            'version' => '3.0.5.0',
            'reference' => '5c095126d1be47e0bb1f92779b7dc099f6feae31',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-krajee-base',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-datetimepicker' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '0882ff90f7c59bb89894b09e31ec7ad2fd8a0107',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
