<?php

namespace api\services;

use backend\models\UserVaiTro;
use common\models\User;

class UserServices
{
    /** Lấy tất cả user
 * @return UserVaiTro[]
 */
    public static function getAllUser(){
        return UserVaiTro::findAll(['status' => 10]);
    }

    /** Lấy user bằng uid và auth
     * @return UserVaiTro
     */
    public static function getUserByUidAuth($uid, $auth){
        return UserVaiTro::findOne(['id' => $uid, 'auth_key' => $auth, 'status' => 10]);
    }
}