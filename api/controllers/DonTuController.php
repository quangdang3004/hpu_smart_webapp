<?php

namespace api\controllers;

use api\models\DuyetDonNghiPhep;
use api\models\DuyetNghiPhep;
use api\models\LichSuDuyetNghiPhep;
use api\models\NghiPhep;
use api\models\NghiPhepPhongBan;
use api\models\PhongBanNhanVien;
use api\models\TrangThaiNghiPhep;
use api\models\User;
use api\models\UserVaiTro;
use common\models\myAPI;
use yii\helpers\Html;
use yii\web\HttpException;

class DonTuController extends AndinCoreApiController
{
    private function convert($date){
        $arr = explode(' ',$date);
        $date = explode('/',$arr[0]);
        $date = implode('-', array_reverse($date));
        return $date.' '.$arr[1];
    }
    private function convertBack($date){
        $arr = explode(' ',$date);
        $date = explode('-',$arr[0]);
        $date = implode('/', array_reverse($date));
        $format = explode(':',$arr[1]);
        $time = $format[0].":".$format[1];
        return $time.' '.$date;
    }

    public function actionYeuCauXinNghi(){
        $arrFides =  ['nghi_tu_ngay','nghi_den_ngay','ly_do' ];
        $donNghi = new NghiPhep();
//        $user = User::findOne($_POST['uid);
        $donNghi->nguoi_lam_don_id = $_POST['uid'];
        $donNghi->user_id = $_POST['uid'];
        $Pb = PhongBanNhanVien::findOne(['nhan_vien_id'=>$_POST['uid'],'active'=>1]);
        if(!is_null($Pb))
            $donNghi->nhan_vien_phong_ban_id =$Pb->id;
        if($this->checkNull('ghi_chu')){
            $donNghi->ghi_chu = $_POST['ghi_chu'];
        }
        $donNghi->ngay_de_nghi = date('Y-m-d H:i:s');
        $_POST['nghi_tu_ngay'] = $this->convert($_POST['nghi_tu_ngay']);
        $_POST['nghi_den_ngay'] = $this->convert($_POST['nghi_den_ngay']);
        foreach ($arrFides as $item){
            if($this->checkNull($item)) {

                $donNghi->{$item} = $_POST[$item];

            }else{
                $this->error500('Vui lòng nhập '.$donNghi->getAttributeLabel($item));
            }
        }
        if($donNghi->save()){
            $trang_thai_don = new TrangThaiNghiPhep();
            $trang_thai_don->nghi_phep_id = $donNghi->id;
            $trang_thai_don->user_id = $donNghi->nguoi_lam_don_id;
            $trang_thai_don->trang_thai = NghiPhep::CHO_DUYET;
            if(!$trang_thai_don->save()){
                $donNghi->updateAttributes(['active'=>0]);
                $this->error500(Html::errorSummary($trang_thai_don));
            }else{
                $tongThoiGianNghi = strtotime($donNghi->nghi_den_ngay) - strtotime($donNghi->nghi_tu_ngay);
                switch ($tongThoiGianNghi){
                    case $tongThoiGianNghi >= DuyetNghiPhep::_5_NGAY :
                        $duyetNew = new DuyetNghiPhep();
                        $duyetNew->user_duyet_id = 1;
                        $duyetNew->trang_thai = 1;
                        $duyetNew->nghi_phep_id= $donNghi->id;
                        if(!$duyetNew->save())
                            $this->error500(Html::errorSummary($duyetNew));
                    case $tongThoiGianNghi >= DuyetNghiPhep::_3_NGAY:
                        if(!is_null($Pb)){
                            $duyetNew = new DuyetNghiPhep();
                            $duyetNew->user_duyet_id = $Pb->phongBan->parent->truong_phong_id;
                            $duyetNew->trang_thai = 1;
                            $duyetNew->nghi_phep_id= $donNghi->id;
                            if(!$duyetNew->save())
                                $this->error500(Html::errorSummary($duyetNew));
                        }
                    case $tongThoiGianNghi >= 1:
                        if(!is_null($Pb)){
                            $duyetNew = new DuyetNghiPhep();
                            $duyetNew->user_duyet_id = $Pb->phongBan->truong_phong_id;
                            $duyetNew->trang_thai = 2;
                            $duyetNew->nghi_phep_id= $donNghi->id;
                            if(!$duyetNew->save())
                                $this->error500(Html::errorSummary($duyetNew));
                        }
                }
//                $duyetNew = new DuyetNghiPhep();
//                $duyetNew->user_duyet_id = 1;
//                $duyetNew->trang_thai = 0;
//                $duyetNew->nghi_phep_id= $donNghi->id;

//                if(!$duyetNew->save())
//                    $this->error500(Html::errorSummary($duyetNew));
                return ['message'=>'Tạo đơn nghỉ phép thành công!'];
            }
        }else{
            $this->error500(Html::errorSummary($donNghi));
        }
    }

    public function actionDanhSachYeuCau(){
        $donNghi = NghiPhepPhongBan::find()
            ->select(['id','ngay_de_nghi','created','updated','trang_thai','id_nguoi_duyet','anh_nguoi_duyet'])
        ->andFilterWhere(['nguoi_lam_don_id'=>$_POST['uid'],'active'=>1]);
        if(isset($_POST['nghiTuNgay'])){
            if($_POST['nghiTuNgay'] !=""){
                $donNghi->andFilterWhere(['>=','ngay_de_nghi',myAPI::convertDMY2YMD($_POST['nghiTuNgay'])]);
            }
        }
        if(isset($_POST['nghiDenNgay'])){
            if($_POST['nghiDenNgay'] !=""){
                $donNghi->andFilterWhere(['>=','ngay_de_nghi',myAPI::convertDMY2YMD($_POST['nghiDenNgay'])]);
            }
        }
        if($this->checkNull('trang_thai')){
            $donNghi->andFilterWhere(['trang_thai'=>$_POST['trang_thai']]);
        }
        $page = ceil($donNghi->count()/20);
        $donNghi = $donNghi->offset(((isset($_POST['page'])?$_POST['page']:1)-1)*20)
            ->limit(20)->orderBy(['created'=>SORT_DESC])
            ->orderBy(['id'=>SORT_DESC])->all();
        foreach ($donNghi as $item){
            $item->ngay_de_nghi = myAPI::covertYMD2DMY($item->ngay_de_nghi);
            if($item->id_nguoi_duyet == null)
                $item->id_nguoi_duyet = [];
            else
                $item->id_nguoi_duyet = explode(',',$item->id_nguoi_duyet);
            if(!is_array($item->id_nguoi_duyet)){
                $item->id_nguoi_duyet = [$item->id_nguoi_duyet];
            }
            if($item->anh_nguoi_duyet == null)
                $item->anh_nguoi_duyet = [];
            else
                $item->anh_nguoi_duyet = explode(',',$item->anh_nguoi_duyet);
            if(!is_array($item->anh_nguoi_duyet)){
                $item->anh_nguoi_duyet = [$item->anh_nguoi_duyet];
            }

        }
        return [
            'tongHop'=> $this->actionTongHop(),
            'so_trang'=>$page,
            'data'=>$donNghi,
        ];
    }

    public function actionChiTietDon(){
        if(!$this->checkNull('id')){
            throw new HttpException(500,'Đơn xin nghỉ không tồn tại trên hệ thống');
        }
        $donYeuCau = NghiPhepPhongBan::findOne(['id'=>$_POST['id'],'active'=>1]);
        if (is_null($donYeuCau))
            throw new HttpException(500,'Đơn xin nghỉ không tồn tại trên hệ thống');
        if ($donYeuCau->nguoi_lam_don_id != $_POST['uid'] && $_POST['uid'] != 1 ){
            if($donYeuCau->id_phong_ban != null)
            {
                $pb = PhongBanNhanVien::findOne(['phong_ban_id'=>$donYeuCau->id_phong_ban,'nhan_vien_id'=>$_POST['uid'],'truong_phong'=>1,'active'=>1]);
                if(is_null($pb)){
                    throw new HttpException(500,'Đơn xin nghỉ không tồn tại trên hệ thống');
                }
            }else{
                throw new HttpException(500,'Đơn xin nghỉ không tồn tại trên hệ thống');
            }
        }
        $nguoiDuyets = [];
        if($donYeuCau->id_nguoi_duyet != null)
        {
            $nguoiDuyets= LichSuDuyetNghiPhep::findAll(['nghi_phep_id'=>$_POST['id']]);
        }
//        $donYeuCau->ngay_de_nghi = myAPI::covertYMD2DMY($donYeuCau->ngay_de_nghi);
//        $donYeuCau->nghi_den_ngay = $this->convertBack($donYeuCau->nghi_den_ngay);
//        $donYeuCau->nghi_tu_ngay = $this->convertBack($donYeuCau->nghi_tu_ngay);
        if($donYeuCau->id_nguoi_duyet == null)
            $donYeuCau->id_nguoi_duyet = [];
        else
            $donYeuCau->id_nguoi_duyet = explode(',',$donYeuCau->id_nguoi_duyet);
        if(!is_array($donYeuCau->id_nguoi_duyet)){
            $donYeuCau->id_nguoi_duyet = [$donYeuCau->id_nguoi_duyet];
        }
        if($donYeuCau->anh_nguoi_duyet == null)
            $donYeuCau->anh_nguoi_duyet = [];
        else
            $donYeuCau->anh_nguoi_duyet = explode(',',$donYeuCau->anh_nguoi_duyet);
        if(!is_array($donYeuCau->anh_nguoi_duyet)){
            $donYeuCau->anh_nguoi_duyet = [$donYeuCau->anh_nguoi_duyet];
        }
        return [
            'data'=>$donYeuCau,
            'nguoi_duyet'=>$nguoiDuyets,
        ];
    }

    public function actionXoaYeuCau(){
        if(!$this->checkNull('id')){
            throw new HttpException(500,'Đơn xin nghỉ không tồn tại trên hệ thống');
        }
        $donYeuCau = NghiPhep::findOne(['id'=>$_POST['id'],'active'=>1]);
        if (is_null($donYeuCau))
            $this->error500('Đơn xin nghỉ không tồn tại trên hệ thống');
        if ($donYeuCau->nguoi_lam_don_id != $_POST['uid'])
            $this->error500('Đơn xin nghỉ không tồn tại trên hệ thống');
        if($donYeuCau->trang_thai != NghiPhep::CHO_DUYET){
            $this->error500('Đơn đã thay đổi trạng thái không thể xoá đi được!');
        }
        $donYeuCau->updateAttributes(['active'=>0]);
        return['message'=>'Cập nhập yêu cầu thành công'];

    }

    public function actionSuaYeuCau(){
        if(!$this->checkNull('id')){
            throw new HttpException(500,'Đơn xin nghỉ không tồn tại trên hệ thống');
        }
        $donYeuCau = NghiPhep::findOne(['id'=>$_POST['id'],'active'=>1]);
        if (is_null($donYeuCau))
            $this->error500('Đơn xin nghỉ không tồn tại trên hệ thống');
        if ($donYeuCau->nguoi_lam_don_id != $_POST['uid'])
            $this->error500('Đơn xin nghỉ không tồn tại trên hệ thống');
        if($donYeuCau->trang_thai != NghiPhep::CHO_DUYET){
            $this->error500('Đơn đã thay đổi trạng thái không thể xoá đi được!');
        }
        $arrFides =  ['nghi_tu_ngay','nghi_den_ngay','ly_do'];
        $arrFidesUpdate = [];
        $_POST['nghi_tu_ngay'] = $this->convert($_POST['nghi_tu_ngay']);
        $_POST['nghi_den_ngay'] = $this->convert($_POST['nghi_den_ngay']);
        $donYeuCau->ngay_de_nghi = date('Y-m-d H:i:s');
        foreach ($arrFides as $item){
            if($this->checkNull($item)){
                $arrFidesUpdate[$item] = $_POST[$item];
            }else{
                $this->error500('Vui lòng điền đầy đủ '.$donYeuCau->getAttributeLabel($item));
            }
        }
        if($this->checkNull('ghi_chu')){
            $arrFidesUpdate['ghi_chu'] = $_POST['ghi_chu'];
        }
        $donYeuCau->updateAttributes($arrFidesUpdate);
        return['message'=>'Cập nhập yêu cầu thành công'];
    }

    public function actionDanhSachDonYeuCauDuyet()
    {
        $vaiTro = UserVaiTro::findOne(['id'=>$_POST['uid']]);
        if($vaiTro->vai_tro_id != 5 && $vaiTro->vai_tro_id != 7 && $vaiTro->id != 1)
            $this->error500('Bạn không có quyền truy cập chức năng này');
        $data = LichSuDuyetNghiPhep::find()->andFilterWhere(['user_duyet_id'=>$_POST['uid']])
//            ->andFilterWhere(['<>','nguoi_lam_don_id',$_POST['uid']
//    ])
;
        if(!isset($_POST['trang_thai'])){
            $data->andFilterWhere(['trang_thai_duyet'=>2]);
        }elseif ($_POST['trang_thai'] == 'Duyệt'){
            $data->andFilterWhere(['trang_thai_duyet'=>3]);
        }elseif ($_POST['trang_thai'] == 'Huỷ'){
            $data->andFilterWhere(['trang_thai_duyet'=>4]);
        }else{
            $data->andFilterWhere(['trang_thai_duyet'=>2]);
        }
        $page = ceil($data->count()/20);
        $data = $data->offset(((isset($_POST['page'])?$_POST['page']:1)-1)*20)
            ->limit(20)->orderBy(['created'=>SORT_DESC])
            ->orderBy(['id'=>SORT_DESC])->all();
        foreach ($data as $item){
            $item->trang_thai = LichSuDuyetNghiPhep::TRANG_THAI[$item->trang_thai_duyet];
        }
        return
            [
                'tongHop'=> $this->actionTongHop(),
            'so_trang'=>$page,
            'data'=>$data
        ];
    }

    public function actionDuyetDon(){
        if(!isset($_POST['id'])){
            $this->error500('Không tìm thấy đơn');
        }
        if(!isset($_POST['type'])){
            $this->error500('Không tìm thấy trạng thái');
        }
        $donYeuCau = NghiPhep::findOne($_POST['id']);
        if(is_null($donYeuCau)){
            $this->error500('Không tìm thấy đơn');
        }
       $duyetYeuCau = DuyetNghiPhep::findOne(['nghi_phep_id'=>$_POST['id'],'user_duyet_id'=>$_POST['uid']]);
        if(is_null($duyetYeuCau)){
            $this->error500('Không tìm thấy đơn');
        }
        if($_POST['type']=='duyet'){
            $duyetYeuCau->trang_thai = 3;
        }elseif($_POST['type']=='huy'){
            $duyetYeuCau->trang_thai = 4;
        }else{
            $this->error500('Không tìm thấy trạng thái');
        }
        if(!$duyetYeuCau->save()){
            $this->error500(Html::errorSummary($duyetYeuCau));
        }
        return[
            'message'=>'Duyệt đơn yêu cầu thành công',
        ];
    }
    public function actionTongHop(){
        $don = NghiPhep::find()->andFilterWhere(['active'=>1]);
        $tongDon = $don->count();
        $choDuyet = $don->where(['active'=>1,'trang_thai'=>'Chờ duyệt','nguoi_lam_don_id'=>$_POST['uid']])->count();
        $daDuyet = $don->where(['active'=>1,'trang_thai'=>'Duyệt','nguoi_lam_don_id'=>$_POST['uid']])->count();
        $daHuy = $don->where(['active'=>1,'trang_thai'=>'Hủy','nguoi_lam_don_id'=>$_POST['uid']])->count();
        return[
          'tongDon' => $tongDon,
            'choDuyet'=> $choDuyet,
            'daDuyet'=>$daDuyet,
            'daHuy'=>$daHuy,
        ];
    }
}