<?php

namespace backend\services;

use backend\helpers\ConstHelper;
use backend\models\PhongBanNhanVien;
use yii\db\ActiveRecord;

class PhongBanNhanVienService
{
    /**
     * Lấy phòng ban nhân viên bởi nhân viên ID
     * @param $nhanVienId int
     * @param $phongBanId int
     * @return PhongBanNhanVien|null|ActiveRecord
     */
    public function getPhongBanNhanVienByNhanVienId($nhanVienId, $phongBanId)
    {
        return PhongBanNhanVien::find()->where(['phong_ban_id' => $phongBanId, 'nhan_vien_id' => $nhanVienId, 'active' => ConstHelper::STATUS_ACTIVE])->one();
    }
}