<?php

use common\models\User;
use yii\helpers\Html;
use common\models\myAPI;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use \backend\models\KeHoachTuyenDung;
use \backend\models\TuyenDungDkNhuCauNs;

?>
<ul class="nav navbar-nav row-fluid" >

    <?php if(myAPI::isAccess2('BinhLuanPhanAnh', 'index')): ?>
        <li>
            <?=Html::a('<i class="fa fa-file"></i> Bình luận phản ánh', Url::to(['binh-luan-phan-anh/index']))?>
        </li>
    <?php endif; ?>
    <?php if(myAPI::isAccess2('Khoa', 'index')): ?>
        <li>
            <?=Html::a('<i class="fa fa-building"></i> Khoa', Url::to(['khoa/index']))?>
        </li>
    <?php endif; ?>
    <?php if(myAPI::isAccess2('Lop', 'index')): ?>
        <li>
            <?=Html::a('<i class="fa fa-building"></i> Lớp', Url::to(['lop/index']))?>
        </li>
    <?php endif; ?>
    <?php if(myAPI::isAccess2('PhongBan', 'index')): ?>
        <li>
            <?=Html::a('<i class="fa fa-building"></i> Phòng ban', Url::to(['phong-ban/index']))?>
        </li>
    <?php endif; ?>
    <?php if(myAPI::isAccess2('ThongBao', 'index')): ?>
        <li>
            <?=Html::a('<i class="fa fa-bell"></i> Thông báo', Url::to(['thong-bao/index']))?>
        </li>
    <?php endif; ?>
    <?php if(myAPI::isAccess2('PhanAnhHienTruong', 'index')): ?>
        <li class="classic-menu-dropdown" aria-haspopup="true">
            <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
                <i class="fa fa-users" ></i> Phản ánh hiện trường<i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu pull-left">
                <li>
                    <?=Html::a('<i class="fa fa-refresh"></i> Phản ánh chờ duyệt <i class="badge badge-pill badge-primary" >10</i>', Url::to(['phan-anh-hien-truong/index']))?>
                </li>
                <li>
                    <?=Html::a('<i class="fa fa-check-circle"></i> Phản ánh đã duyệt <i class="badge badge-pill badge-primary" >10</i>',Url::to(['phan-anh-hien-truong/index']))?>
                </li>
                <li>
                    <?=Html::a('<i class="fa fa-file-text-o"></i> Tất cả', Url::to(['phan-anh-hien-truong/index']))?>
                </li>
            </ul>
        </li>
    <?php endif; ?>
    <?php if(myAPI::isAccess2('TraLoiPhanAnh', 'index')): ?>
        <li>
            <?=Html::a('<i class="fa fa-text-height"></i> Trả lời phản ánh', Url::to(['tra-loi-phan-anh/index']))?>
        </li>
    <?php endif; ?>
    <?php if(myAPI::isAccess2('Cauhinh', 'index') ||
        myAPI::isAccess2('VaiTro', 'index') ||
        myAPI::isAccess2('ChucNang', 'index') ||
        myAPI::isAccess2('User', 'index') ||
        myAPI::isAccess2('DanhMuc', 'Index') ||
        myAPI::isAccess2('PhanQuyen', 'index')): ?>
        <li class="classic-menu-dropdown">
            <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
                <i class="fa fa-cog"></i> Hệ thống <i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu pull-left">
                <?php if(myAPI::isAccess2('Cauhinh', 'index')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-cogs"></i> Cấu hình', Yii::$app->urlManager->createUrl(['cauhinh']))?>
                    </li>
                <?php endif; ?>
                <?php if(myAPI::isAccess2('User', 'index')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-users"></i> Thành viên', Url::to(['user/index']))?>
                    </li>
                <?php endif; ?>
                <?php if(myAPI::isAccess2('VaiTro', 'index')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-users"></i> Vai trò', Yii::$app->urlManager->createUrl(['vai-tro']))?>
                    </li>
                <?php endif; ?>
                <?php if( myAPI::isAccess2('DanhMuc', 'Index')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-file"></i> Danh mục', Url::toRoute('danh-muc/index')) ?>
                    </li>
                <?php endif; ?>
                <?php if(myAPI::isAccess2('ChucNang', 'index')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-bars"></i> Chức năng', Yii::$app->urlManager->createUrl(['chuc-nang']))?>
                    </li>
                <?php endif; ?>

                <?php if(myAPI::isAccess2('PhanQuyen', 'index')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-users"></i> Phân quyền', Yii::$app->urlManager->createUrl(['phan-quyen']))?>
                    </li>
                <?php endif; ?>
            </ul>
        </li>
    <?php endif; ?>
</ul>
