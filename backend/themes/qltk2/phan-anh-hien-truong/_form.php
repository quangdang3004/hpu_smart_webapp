<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PhanAnhHienTruong */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="phan-anh-hien-truong-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'noi_dung')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'khan')->checkbox() ?>

    <?= $form->field($model, 'hinh_anh')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_dinh_kem')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'trang_thai')->dropDownList([ 'Chờ duyệt' => 'Chờ duyệt', 'Duyệt' => 'Duyệt', 'Hủy' => 'Hủy', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'muc_do_phan_anh')->dropDownList([ 'Cấp thiết' => 'Cấp thiết', 'Nguy cấp' => 'Nguy cấp', 'Trung bình' => 'Trung bình', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
