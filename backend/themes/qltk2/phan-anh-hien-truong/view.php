<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PhanAnhHienTruong */
?>
<div class="phan-anh-hien-truong-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'noi_dung:ntext',
            'khan:boolean',
            'hinh_anh',
            'file_dinh_kem',
            'trang_thai',
            'muc_do_phan_anh',
            'created',
            'updated',
            'user_id',
        ],
    ]) ?>

</div>
