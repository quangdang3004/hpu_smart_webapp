<?php

namespace api\controllers;

use backend\helpers\ConstHelper;
use backend\models\AnhPhanAnhHienTruong;
use backend\models\PhanAnhHienTruong;
use backend\models\QuanLyPhanAnhHienTruong;
use backend\models\TraLoiPhanAnh;
use backend\models\TrangThaiPhanAnhHienTruong;
use backend\services\TrangThaiPhanAnhService;
use Mpdf\Tag\Tr;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\HttpException;

class AppPhanAnhHienTruongController extends CoreApiController
{
    public function __construct($id, $module, $config = [])
    {
        $this->allowActions = ['get-data', 'save', 'load', 'delete', 'gui-tra-loi'];
        parent::__construct($id, $module, $config);
    }

    public function actionGetData(){
        $query = QuanLyPhanAnhHienTruong::find();
        if (isset($this->dataPost['fieldsSearch'])){
            if (count($this->dataPost['fieldsSearch']['value']) > 0){
                $arrFieldSearch = ['id', 'muc_do', 'ten_khu_vuc', 'ten_phong_ban', 'trang_thai', 'chuyen_muc'];
                foreach ($arrFieldSearch as $item){
                    if (trim($item) != '') {
                        $query->andFilterWhere(['like', $item, $this->dataPost['fieldsSearch']['value'][$item]]);
                    }
                }
            }
        }
        $totalCount = $query->count();
        $data = $query
            ->orderBy(['id'=> SORT_DESC])
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->limit($this->dataPost['limit'])
            ->andFilterWhere(['active' => 1])
            ->all();

        foreach ($data as $item){
            $hinhAnh = AnhPhanAnhHienTruong::findOne(['phan_anh_id' => $item->id]);
//            VarDumper::dump($item->id);exit();
            if (!is_null($hinhAnh))
                $item->hinh_anh = ConstHelper::BASE_URL.$hinhAnh->image;
        }
        return [
            'results' => $data,
            'rows' => $totalCount
        ];
    }


    /** load */
    public function actionLoad(){
        $model = QuanLyPhanAnhHienTruong::findOne(['id' => $this->dataPost['phan_anh']]);
        $anhPhanAnh = AnhPhanAnhHienTruong::findAll(['phan_anh_id' => $this->dataPost['phan_anh']]);
        foreach ($anhPhanAnh as $item){
            $item->image = ConstHelper::BASE_URL.$item->image;
        }
        return [
            'result' => $model,
            'anhPhanAnh' => $anhPhanAnh
        ];
    }

    /** delete */
    public function actionDelete(){
        $trangThai = TrangThaiPhanAnhService::createTrangThaiPhanAnhWithTrangThaiAndPhanAnhID(PhanAnhHienTruong::HUY, $this->dataPost['phan_anh']);
        if($trangThai)
            return [
                'message' => 'Cập nhật trang thái phản ánh thành công',
            ];
    }

    /** tra loi phan anh */
    public function actionGuiTraLoi(){
        $phanAnhId = $this->dataPost['IdPhanAnh'];

        $traLoiPhanAnh = TraLoiPhanAnh::find()->andWhere(['phan_anh_hien_truong_id' => $phanAnhId])->one();
        if (is_null($traLoiPhanAnh)){
            $traLoiPhanAnh = new TraLoiPhanAnh();
        }

        $traLoiPhanAnh->noi_dung = $this->dataPost['TraLoiPhanAnh'];
        $traLoiPhanAnh->phan_anh_hien_truong_id = $phanAnhId;
//        $traLoiPhanAnh->user_id = \Yii::$app->user->id;
        $traLoiPhanAnh->user_id = $this->dataPost['uid'];
        $traLoiPhanAnh->active = 1;
        TrangThaiPhanAnhService::createTrangThaiPhanAnhWithTrangThaiAndPhanAnhID(PhanAnhHienTruong::DUYET, $phanAnhId);

        if ($traLoiPhanAnh->save(false)){
            if (isset($this->dataPost['ListImg'])){
                if ($this->dataPost['ListImg'] != ''){
                    $anhPhanAnh = $this->dataPost['ListImg'];

                    $anhTraLoiOld = AnhPhanAnhHienTruong::findAll(['tra_loi_phan_anh_id' => $traLoiPhanAnh->id]);

                    foreach ($anhTraLoiOld as $image){
                        $image->delete();
                    }
                    foreach ($anhPhanAnh as $index => $item) {
                        $anh = new AnhPhanAnhHienTruong();
                        $dataImage = preg_replace('/^data:image\/(jpeg|png|gif);base64,/', '', $item);
//                        if(count($dataImage) == 2){
//                            $image = base64_decode(str_replace('PLUSICON', '+', str_replace('RIGHTDASH', '/', $dataImage[1])));
                            $image = base64_decode($dataImage);
                            // Get the image size and type information
                            $image_info = getimagesizefromstring($image);
                            $mime_type = $image_info['mime'];

// Get the file extension from the MIME type
                            $extension = substr($mime_type, strpos($mime_type, '/') + 1);
                            $link = '/images/' . rand(1, time()) . $index .'.'. $extension;
                            $anh->image = $link;
                            file_put_contents(\Yii::getAlias('@root') . $link, $image);
                            $anh->tra_loi_phan_anh_id = $traLoiPhanAnh->id;
//                            VarDumper::dump($anh, 10, true);exit();
                            if (!$anh->save()){
                                return ['message' => Html::errorSummary($anh)];
                            }
                        }
//                    }
                }
            }
            return [
                'status' => 'success',
                'message' => 'Gửi phản ánh thành công'
            ];
        } else {
            return [
                'message' => Html::errorSummary($traLoiPhanAnh)
            ];
        }
    }
}