<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PhongBan */
?>
<div class="phong-ban-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ten_phong_ban',
            'active',
            'truong_phong_id',
        ],
    ]) ?>

</div>
