<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Lop */
?>
<div class="lop-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'si_so',
            'lop_truong_id',
            'khoa_id',
        ],
    ]) ?>

</div>
