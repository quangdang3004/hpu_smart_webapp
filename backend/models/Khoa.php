<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "vq_khoa".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $truong_khoa_id
 *
 * @property User $truongKhoa
 */
class Khoa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vq_khoa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['truong_khoa_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['truong_khoa_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['truong_khoa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'truong_khoa_id' => 'Truong Khoa ID',
        ];
    }

    /**
     * Gets query for [[TruongKhoa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTruongKhoa()
    {
        return $this->hasOne(User::className(), ['id' => 'truong_khoa_id']);
    }
}
