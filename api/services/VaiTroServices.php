<?php
namespace api\services;

use backend\models\VaiTro;

class VaiTroServices
{
    /** Lấy tất cả vai trò
     * @return VaiTro[]
     */
    public static function getAllVaiTro(){
        $vaiTro = VaiTro::findAll(['active' => 1]);
        return $vaiTro;
    }
}