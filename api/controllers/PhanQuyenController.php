<?php

namespace api\controllers;

use backend\controllers\CoreController;
use backend\models\ChucNang;
use backend\models\PhanQuyen;
use backend\models\QuanLyPhanQuyen;
use backend\models\VaiTro;
use common\models\myAPI;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;

class PhanQuyenController extends CoreApiController
{
    public function __construct($id, $module, $config = [])
    {
        $this->allowActions[] = 'get-phan-quyen';
        parent::__construct($id, $module, $config);
    }

    // get-phan-quyen
    public function actionGetPhanQuyen(){
        $chucnang = ChucNang::findAll(['nhom' => $this->dataPost['nhom_chuc_nang']['key']]);
        $vaitro = VaiTro::find()->all();
        $fields = [
            ['key' => 'chuc_nang', 'label' => 'Chức năng', 'width' => '50%']
        ];
        /** @var VaiTro $item */
        foreach ($vaitro as $item) {
            $fields[] = ['key' => $item->ten_vai_tro, 'label' => $item->ten_vai_tro, 'class' => 'text-center', 'width' => '1%'];
        }
        $phanquyen = [];
        $matrixPhanQuyen = [];
        $checked = [];
        $i = 0;
        /** @var ChucNang $item */
        foreach ($chucnang as $item) {
            /** @var VaiTro $item_vaitro */
            $itemPhanQuyen = ['chuc_nang' => $item->name, 'chuc_nang_id' => $item->id, 'chuc_nang_phan_quyen' => []];
            $itemPhanQuyenVaiTro = [];
            $matrixPhanQuyen[$i] = [];
            $j = 0;
            foreach ($vaitro as $item_vaitro) {
                $itemPhanQuyenVaiTro[] = [
                    'vai_tro_id' => $item_vaitro->id,
                    'checked' => !is_null(PhanQuyen::findOne(['chuc_nang_id' => $item->id, 'vai_tro_id' => $item_vaitro->id]))
                ];
                $matrixPhanQuyen[$i][$j] = "{$item->id}_{$item_vaitro->id}";
                $checked[$i][$j] =  !is_null(PhanQuyen::findOne(['chuc_nang_id' => $item->id, 'vai_tro_id' => $item_vaitro->id])) ? "{$item->id}_{$item_vaitro->id}" : '';
                $j++;
            }
            $i++;
            $itemPhanQuyen['chuc_nang_phan_quyen'] = $itemPhanQuyenVaiTro;
            $phanquyen[] = $itemPhanQuyen;
        }

        return [
            'fields' => $fields,
            'phanQuyenMatrix' => $phanquyen,
            'maTrixPhanQuyen' => $matrixPhanQuyen,
            'checked' => $checked
        ];
    }

    public function actionSave(){
        $quanlyphanquyen = QuanLyPhanQuyen::findAll(['nhom' => $this->dataPost['nhom_chuc_nang']['key']]);
        foreach ($quanlyphanquyen as $item) {
            PhanQuyen::deleteAll(['id' => $item->id]);
        }

        if(isset($this->dataPost['checked']['matrix']))
            foreach ($this->dataPost['checked']['matrix'] as $item)
                foreach ($item as $value) {
                    if($value != ''){
                        $arr = explode('_', $value);
                        $phanquyen = new PhanQuyen();
                        $phanquyen->chuc_nang_id = $arr[0];
                        $phanquyen->vai_tro_id = $arr[1];
                        $phanquyen->save();
                    }
                }

        return [
            'message' => 'Đã lưu phân quyền thành công'
        ];
    }
}
